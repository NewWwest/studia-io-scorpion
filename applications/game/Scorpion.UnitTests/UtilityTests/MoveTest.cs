using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using Scorpion.Core.Dev;
using Scorpion.Core.GameMaster;
using Scorpion.Core.Logging;
using Scorpion.Core.Utils;
using Scorpion.Core.Models.Messages.Enums;
using NUnit.Framework;

namespace Scorpion.UnitTests
{
    public class MoveTest
    {
        [Test]
        public void TestMoveException()
        {
            var direction = (Direction)100;
            Assert.Throws<ArgumentOutOfRangeException>(() => MoveUtility.Move(0, 0, direction));
        }

        [Test]
        public void TestMoveDown()
        {
            Direction direction = Direction.Down;
            int oldX = 0;
            int oldY = 1;
            var newPosition = MoveUtility.Move(oldX, oldY, direction);
            Assert.AreEqual(oldX, newPosition.x);
            Assert.AreEqual(oldY - 1, newPosition.y);
        }

        public void TestMoveUp()
        {
            Direction direction = Direction.Up;
            int oldX = 0;
            int oldY = 1;
            var newPosition = MoveUtility.Move(oldX, oldY, direction);
            Assert.AreEqual((oldX, oldY + 1), newPosition);
        }
        [Test]
        public void TestMoveLeft()
        {
            Direction direction = Direction.Left;
            int oldX = 0;
            int oldY = 1;
            var newPosition = MoveUtility.Move(oldX, oldY, direction);
            Assert.AreEqual((oldX - 1, oldY), newPosition);
        }

        [Test]
        public void TestMoveRight()
        {
            Direction direction = Direction.Right;
            int oldX = 0;
            int oldY = 1;
            var newPosition = MoveUtility.Move(oldX, oldY, direction);
            Assert.AreEqual((oldX + 1, oldY), newPosition);
        }
    }
}
