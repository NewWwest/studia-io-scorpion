﻿using Scorpion.Core;
using Scorpion.Core.Dev;
using Scorpion.Core.Exceptions;
using Scorpion.Core.Logging;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Networking;
using Scorpion.Core.Player;
using System;

namespace Scorpion.Player
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            try
            {
                var config = new TechnicalConfiguration();
                var xd = new GameRunner();
                xd.RunPlayer(config.Host, config.Port, Core.Models.Team.Bottom);
            }
            catch (PlayerWasRefusedConnectionException)
            {
                Console.WriteLine("PlayerWasRefusedConnection");
            }
            Console.ReadLine();
        }
    }
}
