﻿using System;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Scorpion.Core.Models.Messages.Enums;
using Scorpion.Core.Models.Messages.Errors;
using Scorpion.Core.Networking;
using Scorpion.Core.Logging;
using Action = Scorpion.Core.Models.Messages.Actions.Action;
using Scorpion.Core.Utils;
using System.Threading;
using Scorpion.Core.Models;

namespace Scorpion.Core.Player
{
    public class RandomlyMovingPlayer : PlayerAgent
    {
        Random _random;
        private Direction _currentDirection;

        public RandomlyMovingPlayer(Team teamId, IChannel channel, ILogger logger) : base(teamId, channel, logger)
        {
            _random = new Random();
        }

        protected override Action ProcessAndThink(Message returnedMessage)
        {
            if (returnedMessage is CommunicationRequestForwardData dataRequest)
            {
                return new CommunicationAgreementAction() { AgentId = AgentId, Agreement = true, Data = "Koty sa spoko", WithAgentId = dataRequest.AgentId };
            }
            if (returnedMessage is Error e)
            {
                switch (e)
                {
                    case GameMasterNotConnectedYetError response:
                        throw new Exception("This should not happen at demo-1 stage");
                    case CantMoveInThisDirectionError error:
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case GameMasterNotRespondingError error:
                        throw new Exception("This should not happen at demo-1 stage");
                    case InvalidActionError error:
                    case InvalidJsonError error1:
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case RequestDuringTimePenaltyError error2:
                        Thread.Sleep(error2.WaitUntilTime - error2.Timestamp);
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    default:
                        throw new NotImplementedException("Implement more errors heh?");
                }
            }
            else if (returnedMessage is Response r)
            {
                Thread.Sleep(r.WaitUntilTime - r.Timestamp);
                switch (r)
                {
                    case CheckPieceResponse response:
                        if (!response.IsCorrect)
                        {
                            return new DestroyPieceAction() { AgentId = AgentId };
                        }
                        else
                        {
                            _currentDirection = goalAreaDirection;
                            return new MakeMoveAction() { AgentId = AgentId, MoveDirection = goalAreaDirection };
                        }
                    case CommunicationResponse response:
                        //Information? lol I DONT CARE
                        return null;
                    case DestroyPieceResponse response:
                        Destroy(response);
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case DiscoveryResponse response:
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case MakeMoveResponse response:
                        Move(response, _currentDirection);
                        if (_player.Piece != null)
                        {
                            if (IsInGoalArea())
                            {
                                if (_blocks[_player.MyPositionX, _player.MyPositionY].State == Models.BlockState.DiscoveredNonGoal ||
                                    _blocks[_player.MyPositionX, _player.MyPositionY].State == Models.BlockState.DiscoveredGoal)
                                {
                                    _currentDirection = RandomDirection();
                                    return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                                }
                                else
                                {
                                    return new PutPieceAction() { AgentId = AgentId };
                                }
                            }
                            else
                            {
                                _currentDirection = goalAreaDirection;
                                return new MakeMoveAction() { AgentId = AgentId, MoveDirection = goalAreaDirection };
                            }
                        }
                        else if (response.ClosestPiece == 0)
                        {
                            return new PickPieceAction() { AgentId = AgentId };
                        }
                        else
                        {
                            _currentDirection = RandomDirection();
                            return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                        }
                    case PickPieceResponse response:
                        Pick(response);
                        return new CheckPieceAction() { AgentId = AgentId };
                    case PutPieceResponse response:
                        Put(response);
                        _currentDirection = RandomDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    default:
                        throw new NotImplementedException("impllement more actions heh?");
                }
            }

            return null;
        }

        private Direction RandomDirectionInGoalArea()
        {
            var dir = RandomDirection();
            while (dir == MoveUtility.GetOppositeDirection(goalAreaDirection))
            {
                dir = RandomDirection();
            }
            return dir;
        }

        private Direction RandomDirection()
        {
            var direction = (Direction)_random.Next(4);
            return direction;
        }
    }
}
