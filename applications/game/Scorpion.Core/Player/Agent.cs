﻿using System;
using System.Collections.Generic;
using System.Threading;
using Scorpion.Core.GameMaster;
using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Scorpion.Core.Models.Messages.Enums;
using Scorpion.Core.Models.Messages.Info;
using Scorpion.Core.Networking;
using Action = Scorpion.Core.Models.Messages.Actions.Action;
using System.Linq;
using Scorpion.Core.Utils;
using Scorpion.Core.Models.Messages.Setup;
using Scorpion.Core.Models.Messages.Errors;
using Scorpion.Core.Exceptions;

namespace Scorpion.Core.Player
{
    public abstract class PlayerAgent
    {
        protected PlayerClass _player;

        protected readonly IChannel _channel;
        protected readonly ILogger _logger;

        public int AgentId => _player.AgentId;
        public Team TeamId => _player.TeamId;

        protected bool _isLeader;
        protected BlockIllusion[,] _blocks;
        protected List<int> teamPlayersId;
        protected GameStartInfo gameInfo;
        protected Direction goalAreaDirection;

        public PlayerAgent(Team teamId, IChannel channel, ILogger logger)
        {
            _player = new PlayerClass();
            _channel = channel;
            _player.TeamId = teamId;
            _logger = logger;
        }

        protected abstract Action ProcessAndThink(Message returnedMessage);

        public void Run()
        {
            ConnectToServer();
            MakeMoveAction firstMove = WaitForGameStartSignal();
            _channel.SendMessage(firstMove);

            while (true)
            {
                Message returnedMessage = null;
                try
                {
                    returnedMessage = _channel.GetMessage();
                }
                catch (SerializationException e)
                {
                    _logger.Log($"A{AgentId}: Received bad JSON: {e}");
                    continue;
                }

                _logger.Log($"A{AgentId}: Received message of type {returnedMessage}");

                if (returnedMessage is GameOverInfo x1)
                    throw new GameEndedException(x1.WinningTeam);
                if (returnedMessage is GameMasterNotRespondingError x2)
                    throw new GameMasterNotRespondingException();

                var nextAction = ProcessAndThink(returnedMessage);
                Log(nextAction);

                if (nextAction == null)
                    continue; //Read next message

                _channel.SendMessage(nextAction);
            }
        }

        private void ConnectToServer()
        {
            var request = new PlayerJoinRequest()
            {
                AgentId = _player.AgentId,
                TeamId = _player.TeamId,
                WantToBeLeader = true
            };
            _channel.SendMessage(request);
            var response = _channel.GetMessage();
            if (response is Error e)
            {
                throw new PlayerWasRefusedConnectionException(e);
            }
            if (response is PlayerJoinResponse resp)
            {
                if (!resp.IsConnected)
                    throw new PlayerWasRefusedConnectionException();

                _player.AgentId = resp.AgentId;
            }
        }

        private MakeMoveAction WaitForGameStartSignal()
        {
            _logger.Log($"A{AgentId}: Player {AgentId} waiting for game to start");
            var msg = _channel.GetMessage();
            if (msg is GameStartInfo info)
            {
                gameInfo = info;
                _player.MyPositionX = info.InitialXPosition;
                _player.MyPositionY = info.InitialYPosition;
                _blocks = new BlockIllusion[info.BoardSizeX, info.BoardSizeY];
                for (int x = 0; x < info.BoardSizeX; x++)
                {
                    for (int y = 0; y < info.BoardSizeY; y++)
                    {
                        _blocks[x, y] = new BlockIllusion(x, y);
                    }
                }

                if (AgentId == info.TeamLeaderId)
                    _isLeader = true;

                if (info.BoardSizeY / 2 > info.InitialYPosition)
                    goalAreaDirection = Direction.Down;
                else
                    goalAreaDirection = Direction.Up;

                Direction moveDirection;
                if (goalAreaDirection == Direction.Down)
                    moveDirection = Direction.Up;
                else
                    moveDirection = Direction.Down;

                return new MakeMoveAction() { AgentId = AgentId, MoveDirection = moveDirection };
            }
            else
            {
                throw new Exception("Expected game start info signal");
            }
        }

        private void Log(Message nextAction)
        {
            if (nextAction == null)
                _logger.Log($"A{AgentId}: Decided to take no action");
            else
                _logger.Log($"A{AgentId}: Decided to take action: {nextAction}");
        }

        protected void Move(MakeMoveResponse response, Direction direction)
        {
            _player.MyPositionX = response.NewXPosition;
            _player.MyPositionY = response.NewYPosition;
        }

        protected void Pick(PickPieceResponse response)
        {
            _player.Piece = new Piece();
        }

        protected void Put(PutPieceResponse response)
        {
            switch (response.Result)
            {
                case PiecePlacementResult.BackInTaskArea:
                    break;
                case PiecePlacementResult.GoalCompleted:
                    _blocks[_player.MyPositionX, _player.MyPositionY].State = BlockState.DiscoveredGoal;
                    break;
                case PiecePlacementResult.GoalNotCompleted:
                    _blocks[_player.MyPositionX, _player.MyPositionY].State = BlockState.DiscoveredNonGoal;
                    break;
                case PiecePlacementResult.PieceWasFake:
                    break;
                default:
                    break;
            }
            _player.Piece = null;
        }

        protected void Destroy(DestroyPieceResponse response)
        {
            _player.Piece = null;
        }

        protected void Discover(DiscoveryResponse response)
        {
            foreach (var (x, y, dist) in response.ClosestPieces)
            {
                _blocks[x, y].DistanceForAgent = dist;
            }
        }

        protected bool IsInGoalArea()
        {
            if (goalAreaDirection == Direction.Up)
            {
                if (_player.MyPositionY >= gameInfo.BoardSizeY - gameInfo.GoalAreaHeight)
                    return true;
                else
                    return false;
            }
            else
            {
                if (_player.MyPositionY < gameInfo.GoalAreaHeight)
                    return true;
                else
                    return false;
            }
        }
    }
}
