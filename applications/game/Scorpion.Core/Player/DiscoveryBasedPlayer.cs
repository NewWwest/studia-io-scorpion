﻿using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Scorpion.Core.Models.Messages.Enums;
using Scorpion.Core.Models.Messages.Errors;
using Scorpion.Core.Networking;
using Scorpion.Core.Utils;
using System;
using System.Threading;
using Action = Scorpion.Core.Models.Messages.Actions.Action;

namespace Scorpion.Core.Player
{
    public class DiscoveryBasedPlayer : PlayerAgent
    {
        Random _random;
        private Direction _currentDirection;
        public DiscoveryBasedPlayer(Team teamId, IChannel channel, ILogger logger) : base(teamId, channel, logger)
        {
            _random = new Random();
        }

        protected override Action ProcessAndThink(Message returnedMessage)
        {
            if (returnedMessage is CommunicationRequestForwardData dataRequest)
            {
                return new CommunicationAgreementAction() { AgentId = AgentId, Agreement = true, Data = "Koty sa spoko", WithAgentId = dataRequest.AgentId };
            }
            if (returnedMessage is Error e)
            {
                switch (e)
                {
                    case GameMasterNotConnectedYetError response:
                        throw new Exception("This should not happen at demo-1 stage");
                    case CantMoveInThisDirectionError error:
                        //_currentDirection = RandomDirection();
                        //return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                        return new DiscoveryAction { AgentId = AgentId };
                    case GameMasterNotRespondingError error:
                        throw new Exception("This should not happen at demo-1 stage");
                    case InvalidActionError error:
                    case InvalidJsonError error1:
                    case RequestDuringTimePenaltyError error2:
                        //aka WTF Happend
                        _currentDirection = GetDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    default:
                        throw new NotImplementedException("Implement more errors heh?");
                }
            }
            else if (returnedMessage is Response r)
            {
                Thread.Sleep(r.WaitUntilTime - r.Timestamp);
                switch (r)
                {
                    case CheckPieceResponse response:
                        if (!response.IsCorrect)
                        {
                            return new DestroyPieceAction() { AgentId = AgentId };
                        }
                        else
                        {
                            _currentDirection = goalAreaDirection;
                            return new MakeMoveAction() { AgentId = AgentId, MoveDirection = goalAreaDirection };
                        }
                    case CommunicationResponse response:
                        //Information? lol I DONT CARE
                        return null;
                    case DestroyPieceResponse response:
                        Destroy(response);
                        _currentDirection = GetDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case DiscoveryResponse response:
                        Discover(response);
                        _currentDirection = GetDirection();
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    case MakeMoveResponse response:
                        Move(response, _currentDirection);
                        if (_player.Piece != null)
                        {
                            if (IsInGoalArea())
                            {
                                if (_blocks[_player.MyPositionX, _player.MyPositionY].State == Models.BlockState.DiscoveredNonGoal ||
                                    _blocks[_player.MyPositionX, _player.MyPositionY].State == Models.BlockState.DiscoveredGoal)
                                {
                                    return new DiscoveryAction { AgentId = AgentId };
                                }
                                else
                                {
                                    return new PutPieceAction() { AgentId = AgentId };
                                }
                            }
                            else
                            {
                                _currentDirection = goalAreaDirection;
                                return new MakeMoveAction() { AgentId = AgentId, MoveDirection = goalAreaDirection };
                            }
                        }
                        else if (response.ClosestPiece == 0)
                        {
                            return new PickPieceAction() { AgentId = AgentId };
                        }
                        else if (IsInGoalArea())
                        {
                            _currentDirection = MoveUtility.GetOppositeDirection(goalAreaDirection);
                            return new MakeMoveAction() { AgentId = AgentId, MoveDirection = goalAreaDirection };

                        }
                        else
                        {
                            return new DiscoveryAction { AgentId = AgentId };
                        }
                    case PickPieceResponse response:
                        Pick(response);
                        return new CheckPieceAction() { AgentId = AgentId };
                    case PutPieceResponse response:
                        Put(response);
                        _currentDirection = MoveUtility.GetOppositeDirection(goalAreaDirection);
                        return new MakeMoveAction() { AgentId = AgentId, MoveDirection = _currentDirection };
                    default:
                        throw new NotImplementedException("impllement more actions heh?");
                }
            }

            return null;
        }


        private Direction GetDirection()
        {
            (int distance, int x, int y, int lvl) bestField = (int.MaxValue, -1, -1, -1);
            (int distance, int x, int y) bestFieldAtLvl = (int.MaxValue, -1, -1);
            int lvl = 1;
            while (lvl < bestField.distance)
            {
                bestFieldAtLvl = CheckFieldsAroundAtLvl(lvl);
                if (bestFieldAtLvl.distance + lvl < bestField.distance + bestField.lvl)
                {
                    bestField.distance = bestFieldAtLvl.distance;
                    bestField.x = bestFieldAtLvl.x;
                    bestField.y = bestFieldAtLvl.y;
                    bestField.lvl = lvl;
                }

                lvl++;
            }
            if (bestField.y > _player.MyPositionY)
                return Direction.Up;
            if (bestField.y < _player.MyPositionY)
                return Direction.Down;
            if (bestField.x > _player.MyPositionX)
                return Direction.Right;
            return Direction.Left;
        }

        private (int, int, int) CheckFieldsAroundAtLvl(int lvl)
        {
            int minDistance = int.MaxValue;
            (int x, int y) field = (-1, -1);
            int? distance;
            foreach (var block in ArrayUtility.ElementsInRing(lvl, _player.MyPositionX, _player.MyPositionY, gameInfo.GoalAreaHeight, _blocks))
            {
                distance = block.DistanceForAgent;
                if (distance != null && distance < minDistance)
                {
                    minDistance = (int)distance;
                    field = (block.X, block.Y);
                }
            }
            return (minDistance, field.x, field.y);
        }


        
    }
}
