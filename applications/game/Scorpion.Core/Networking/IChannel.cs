﻿using Scorpion.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Networking
{
    public interface IChannel
    {
        void SendMessage(Message requestMessage);

        Message GetMessage();
    }
}
