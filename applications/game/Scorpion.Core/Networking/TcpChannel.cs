﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Scorpion.Core.Exceptions;
using Scorpion.Core.Logging;
using Scorpion.Core.Models.Messages;

namespace Scorpion.Core.Networking
{
    public class TcpChannel : IChannel, IDisposable
    {
        private readonly TcpClient _client;
        private readonly NetworkStream _dataStream;
        private readonly ILogger _logger;
        private readonly ISerializer _serializer;

        public TcpChannel(string server, int port, ILogger logger, ISerializer serializer)
        {
            _client = new TcpClient(server, port);
            _dataStream = _client.GetStream();
            _logger = logger ;
            _serializer = serializer;
        }

        public void Dispose()
        {
            _dataStream.Close();
            _client.Close();
            _dataStream.Dispose();
            _client.Dispose();
        }

        public void SendMessage(Message requestMessage)
        {
            byte[] request = _serializer.Serialize(requestMessage);
            _logger.Log($"Sending: {string.Join(" ", request)}");
            _dataStream.Write(request, 0, request.Length);
        }

        public Message GetMessage()
        {
            Message message=null;
            try
            {
                var bytes = new byte[4];

                _dataStream.Read(bytes, 0, 4);
                if (BitConverter.IsLittleEndian)
                {
                    _logger.Log("We are on a little endian system - inverting bytes");
                    Array.Reverse(bytes);
                }
                int length = BitConverter.ToInt32(bytes, 0);
                _logger.Log($"Received: length:{length} (bytes:{string.Join(" ", bytes)})");

                _dataStream.Read(bytes, 0, 4);
                if (BitConverter.IsLittleEndian)
                {
                    _logger.Log("We are on a little endian system - inverting bytes");
                    Array.Reverse(bytes);
                }
                int code = BitConverter.ToInt32(bytes, 0);
                _logger.Log($"Received: id:{code} (bytes:{string.Join(" ", bytes)})");

                byte[] data = new byte[length];
                _dataStream.Read(data, 0, length);
                _logger.Log($"Received: length:{length} | ID:{code}");
                _logger.Log($"Received: {Encoding.UTF8.GetString(data)}");

                message = _serializer.Deserialize(data);
                message.Validate();
                
                if (!Enum.IsDefined(typeof(MessageType), code))
                    throw new SerializationException($"Unexpected MsgId value int:{code} bytes:{string.Join(" ", bytes)} (previosly read length:{length})");

                if (code != (int)message.MsgId)
                    throw new SerializationException($"Missmatch of MsgIds: in json:{(int)message.MsgId}; in header:{code}");

                return message;
            }
            catch (SerializationException e)
            {
                _logger.Log($"Decodding message failed {e}");
                throw;
            }
            catch (Exception exc)
            {
                _logger.Log($"Decodding message failed {exc}");
                try
                {
                    int agentId = ((dynamic)message).AgentId;
                    throw new SerializationException(agentId, $"Decodding message failed. See inner exception for details.", exc);
                }
                catch (Exception)
                {
                    throw new SerializationException($"Decodding message failed. See inner exception for details.", exc);
                }
            }
        }
    }
}
