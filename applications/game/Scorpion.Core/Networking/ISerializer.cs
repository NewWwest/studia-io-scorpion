﻿using Scorpion.Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Networking
{
    public interface ISerializer
    {
        byte[] Serialize(Message message);
        Message Deserialize(byte[] data);
    }
}
