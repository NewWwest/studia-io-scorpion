﻿using Newtonsoft.Json;
using Scorpion.Core.Models.Messages;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Text;
using Scorpion.Core.Models.Messages.Errors;
using Scorpion.Core.Models.Messages.Info;
using Scorpion.Core.Models.Messages.Setup;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Newtonsoft.Json.Serialization;

namespace Scorpion.Core.Networking
{
    public class SimpleJsonSerializer : ISerializer
    {
        public Message Deserialize(byte[] data)
        {
            string json = Encoding.UTF8.GetString(data);
            JObject jObject = JObject.Parse(json);
            switch ((MessageType)jObject["msgId"].Value<int>())
            {
                case MessageType.AgentNotRespondingError:
                    return JsonConvert.DeserializeObject<AgentNotRespondingError>(json);
                case MessageType.CantMoveInThisDirectionError:
                    return JsonConvert.DeserializeObject<CantMoveInThisDirectionError>(json);
                case MessageType.GameMasterNotConnectedYetError:
                    return JsonConvert.DeserializeObject<GameMasterNotConnectedYetError>(json);
                case MessageType.GameMasterNotRespondingError:
                    return JsonConvert.DeserializeObject<GameMasterNotRespondingError>(json);
                case MessageType.InvalidActionError:
                    return JsonConvert.DeserializeObject<InvalidActionError>(json);
                case MessageType.InvalidJsonError:
                    return JsonConvert.DeserializeObject<InvalidJsonError>(json);
                case MessageType.RequestDuringTimePenaltyError:
                    return JsonConvert.DeserializeObject<RequestDuringTimePenaltyError>(json);
                case MessageType.WybranoCalyPrzedzialDlaPsaError:
                    return JsonConvert.DeserializeObject<WybranoCalyPrzedzialDlaPsaError>(json);
                case MessageType.GameOverInfo:
                    return JsonConvert.DeserializeObject<GameOverInfo>(json);
                case MessageType.GameStartInfo:
                    return JsonConvert.DeserializeObject<GameStartInfo>(json);
                case MessageType.GameMasterJoinRequest:
                    return JsonConvert.DeserializeObject<GameMasterJoinRequest>(json);
                case MessageType.GameMasterJoinResponse:
                    return JsonConvert.DeserializeObject<GameMasterJoinResponse>(json);
                case MessageType.PlayerJoinRequest:
                    return JsonConvert.DeserializeObject<PlayerJoinRequest>(json);
                case MessageType.PlayerJoinResponse:
                    return JsonConvert.DeserializeObject<PlayerJoinResponse>(json);
                case MessageType.CheckPieceAction:
                    return JsonConvert.DeserializeObject<CheckPieceAction>(json);
                case MessageType.CommunicationAgreementAction:
                    return JsonConvert.DeserializeObject<CommunicationAgreementAction>(json);
                case MessageType.CommunicationRequestData:
                    return JsonConvert.DeserializeObject<CommunicationRequestData>(json);
                case MessageType.CommunicationRequestForwardData:
                    return JsonConvert.DeserializeObject<CommunicationRequestForwardData>(json);
                case MessageType.DestroyPieceAction:
                    return JsonConvert.DeserializeObject<DestroyPieceAction>(json);
                case MessageType.DiscoveryAction:
                    return JsonConvert.DeserializeObject<DiscoveryAction>(json);
                case MessageType.MakeMoveAction:
                    return JsonConvert.DeserializeObject<MakeMoveAction>(json);
                case MessageType.PickPieceAction:
                    return JsonConvert.DeserializeObject<PickPieceAction>(json);
                case MessageType.PutPieceAction:
                    return JsonConvert.DeserializeObject<PutPieceAction>(json);
                case MessageType.CheckPieceResponse:
                    return JsonConvert.DeserializeObject<CheckPieceResponse>(json);
                case MessageType.CommunicationResponse:
                    return JsonConvert.DeserializeObject<CommunicationResponse>(json);
                case MessageType.DestroyPieceResponse:
                    return JsonConvert.DeserializeObject<DestroyPieceResponse>(json);
                case MessageType.DiscoveryResponse:
                    return JsonConvert.DeserializeObject<DiscoveryResponse>(json);
                case MessageType.MakeMoveResponse:
                    return JsonConvert.DeserializeObject<MakeMoveResponse>(json);
                case MessageType.PickPieceResponse:
                    return JsonConvert.DeserializeObject<PickPieceResponse>(json);
                case MessageType.PutPieceResponse:
                    return JsonConvert.DeserializeObject<PutPieceResponse>(json);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public byte[] Serialize(Message message)
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            string json = JsonConvert.SerializeObject(message, serializerSettings);
            var jsonBytes = Encoding.UTF8.GetBytes(json);
            var lengthBytes = BitConverter.GetBytes(jsonBytes.Length);
            Array.Reverse(lengthBytes);
            var idBytes = BitConverter.GetBytes((int)message.MsgId);
            Array.Reverse(idBytes);

            var output = new byte[lengthBytes.Length + idBytes.Length + jsonBytes.Length];
            lengthBytes.CopyTo(output, 0);
            idBytes.CopyTo(output, lengthBytes.Length);
            jsonBytes.CopyTo(output, lengthBytes.Length + idBytes.Length);

            return output;
        }
    }
}
