﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Logging
{
    public interface ILogger
    {
        void Log(string message, int level=0);
    }
}
