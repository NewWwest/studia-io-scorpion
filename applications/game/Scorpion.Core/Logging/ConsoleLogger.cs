﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string message, int level = 0)
        {
            //if (message.StartsWith("GM") && message.Contains("Player1"))
                Console.WriteLine(message);
        }
    }
}
