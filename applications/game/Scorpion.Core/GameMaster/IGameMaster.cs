﻿namespace Scorpion.Core.GameMaster
{
    public interface IGameMaster
    {
        void Run();
    }
}