﻿using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Models.Messages.Enums;
using Scorpion.Core.Utils;
using System;
using System.Collections.Generic;

namespace Scorpion.Core.GameMaster
{
    public class Board
    {
        public Block[,] Blocks => _blocks;
        public GameConfiguration GameConfiguration => _gameConfiguration;

        private readonly Block[,] _blocks;
        private readonly GameConfiguration _gameConfiguration;
        private readonly ILogger _logger;
        private readonly Random _random;
        private int currentPieces;

        private int BoardX => _gameConfiguration.BoardSizeX;
        private int BoardY => _gameConfiguration.TaskAreaHeight + _gameConfiguration.GoalAreaHeight * 2;

        public Board(ILogger logger)
        {
            _logger = logger;
            _random = new Random();
            _gameConfiguration = new GameConfiguration();
            _blocks = new Block[BoardX, BoardY];

            for (int x = 0; x < BoardX; x++)
            {
                for (int y = 0; y < BoardY; y++)
                {
                    _blocks[x, y] = new Block(x, y);
                }
            }
            for (int i = 0; i < _gameConfiguration.InitialPieces; i++)
            {
                TrySpawnPiece();
            }
            for (int i = 0; i < _gameConfiguration.NumberOfGoals; i++)
            {
                AddGoal();
            }
        }
        
        internal void TrySpawnPiece()
        {
            if (currentPieces >= _gameConfiguration.MaxPieceNumber)
                return;

            int x = -1, y = -1;
            for (int i = 0; i < 100; i++)
            {
                x = _random.Next(BoardX);
                y = _random.Next(_gameConfiguration.TaskAreaHeight);
                if (_blocks[x, _gameConfiguration.GoalAreaHeight + y].Piece == null)
                    break;
            }
            if (x == -1 || y == -1)
                return;

            _logger.Log($"GM: Spawning piece at ({x},{_gameConfiguration.GoalAreaHeight + y})", 1);
            _blocks[x, _gameConfiguration.GoalAreaHeight + y].Piece = new Piece()
            {
                Valid = _random.Next(100) < _gameConfiguration.ValidPiecePercentage
            };
            currentPieces++;
        }

        internal IList<(int x, int y, int dist)> Discovery(int PositionX, int PositionY)
        {
            IList<(int x, int y, int dist)> result = new List<(int x, int y, int dist)>();
            for (int dx = -1; dx <= 1; dx++)
            {
                for (int dy = -1; dy <= 1; dy++)
                {
                    int x = PositionX + dx;
                    int y = PositionY + dy;
                    if (0 <= x &&
                        x < BoardX &&
                        _gameConfiguration.GoalAreaHeight <= y &&
                        y < _gameConfiguration.GoalAreaHeight + _gameConfiguration.TaskAreaHeight)
                    {
                        result.Add((x, y, DistanceToClosestPiece(x, y)));
                    }
                }
            }
            return result;
        }
        
        internal bool TryMovePlayer(int x, int y, out int distanceToPiece)
        {
            distanceToPiece = DistanceToClosestPiece(x, y);
            return true;
        }

        internal void DestoyPiece()
        {
            currentPieces--;
        }

        internal bool TryPickUpPiece(int PositionX, int PositionY, out Piece piece)
        {
            piece = _blocks[PositionX, PositionY].Piece;
            _blocks[PositionX, PositionY].Piece = null;
            return piece != null;
        }

        internal bool TryPutPiece(int x, int y, Piece piece, out PiecePlacementResult result)
        {
            if (y < _gameConfiguration.GoalAreaHeight || _gameConfiguration.GoalAreaHeight + _gameConfiguration.TaskAreaHeight <= y)
            {
                if (!piece.Valid.Value)
                {
                    result = PiecePlacementResult.PieceWasFake;
                    currentPieces--;
                    return true;
                }
                switch (_blocks[x,y].State)
                {
                    case BlockState.DiscoveredNonGoal:
                    case BlockState.Default:
                        _blocks[x, y].State = BlockState.DiscoveredNonGoal;
                        result = PiecePlacementResult.GoalNotCompleted;
                        currentPieces--;
                        return true;
                    case BlockState.Goal:
                    case BlockState.DiscoveredGoal:
                        _blocks[x, y].State = BlockState.DiscoveredGoal;
                        result = PiecePlacementResult.GoalCompleted;
                        currentPieces--;
                        return true;
                    default:
                        throw new Exception();
                }
            }
            else
            {
                if (_blocks[x, y].Piece != null)
                {
                    result = (PiecePlacementResult)(-1); //For compilation
                    return false;
                }
                else
                {
                    _blocks[x, y].Piece = piece;
                    result = PiecePlacementResult.BackInTaskArea;
                    return true;
                }
            }
        }

        internal Team? CheckForWinners()
        {
            bool bottomTeamHasNonDiscoveredGoals = false;
            for (int x = 0; x < BoardX; x++)
            {
                for (int y = 0; y < _gameConfiguration.GoalAreaHeight; y++)
                {
                    if (_blocks[x, y].State == BlockState.Goal)
                    {
                        bottomTeamHasNonDiscoveredGoals = true;
                        break;
                    }
                }
                if (bottomTeamHasNonDiscoveredGoals)
                    break;
            }
            if (!bottomTeamHasNonDiscoveredGoals)
                return Team.Bottom;


            bool upperTeamHasNonDiscoveredGoals = false;
            for (int x = 0; x < BoardX; x++)
            {
                for (int y = _gameConfiguration.GoalAreaHeight + _gameConfiguration.TaskAreaHeight; y < BoardY; y++)
                {
                    if (_blocks[x, y].State == BlockState.Goal)
                    {
                        upperTeamHasNonDiscoveredGoals = true;
                        break;
                    }
                }
                if (upperTeamHasNonDiscoveredGoals)
                    break;
            }
            if (!upperTeamHasNonDiscoveredGoals)
                return Team.Upper;

            return null;
        }


        private int DistanceToClosestPiece(int x, int y)
        {
            int mininum = int.MaxValue;
            bool pieceWasFound = false;
            for (int i = 0; i < 2 * Math.Max(BoardX, BoardY); i++)
            {
                foreach (var block in ArrayUtility.ElementsInRing(i, x, y, 0, _blocks))
                {
                    if (block != null && block.Piece != null)
                    {
                        pieceWasFound = true;
                        int distance = Math.Abs(x - block.X) + Math.Abs(y - block.Y);
                        if (mininum > distance)
                            mininum = distance;
                    }
                }
                if (pieceWasFound)
                    return mininum;
            }
            if (mininum == int.MaxValue)
                return -1;

            return mininum;
        }

        private void AddGoal()
        {
            int x, y;
            while (true)
            {
                x = _random.Next(BoardX);
                y = _random.Next(_gameConfiguration.GoalAreaHeight);
                if (_blocks[x, y].State != BlockState.Goal)
                    break;
            }
            _blocks[x, y].State = BlockState.Goal;
            _blocks[x, BoardY - y - 1].State = BlockState.Goal;
        }
    }
}