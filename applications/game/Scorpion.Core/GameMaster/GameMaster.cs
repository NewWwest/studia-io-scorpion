﻿using Scorpion.Core.Exceptions;
using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Models.Messages.Actions;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Scorpion.Core.Models.Messages.Errors;
using Scorpion.Core.Models.Messages.Info;
using Scorpion.Core.Models.Messages.Setup;
using Scorpion.Core.Networking;
using Scorpion.Core.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Action = Scorpion.Core.Models.Messages.Actions.Action;

namespace Scorpion.Core.GameMaster
{
    public class GameMasterAgent : IGameMaster
    {
        private readonly IChannel _channel;
        private readonly ILogger _logger;
        private readonly GameConfiguration _gameConfiguration;

        private bool _gameInProgress = false;
        private List<PlayerClass> _players = new List<PlayerClass>();
        private List<CommunicationRequestData> _queuedDataExchanges = new List<CommunicationRequestData>(); //Communication not supported yet
        private Board _board;
        private readonly DateTime _timeZero;

        public event EventHandler BoardChanged;

        public GameMasterAgent(IChannel channel, ILogger logger)
        {
            _channel = channel;
            _logger = logger;
            _gameConfiguration = new GameConfiguration();
            _timeZero = DateTime.Now;
        }

        protected virtual void OnBoardChanged(BoardChangedEventArgs e)
        {
            BoardChanged?.Invoke(this, e);
        }

        public void Run()
        {
            _channel.SendMessage(new GameMasterJoinRequest());
            Message resp = _channel.GetMessage();
            if (resp is GameMasterJoinResponse)
            {
                _board = new Board(_logger);
                RunGame();
            }
            else
            {
                throw new Exception("Game master was refused connecting to server");
            }
        }

        private void RunGame()
        {
            Message message = null;
            _logger.Log($"GM: Waiting for players.");
            try
            {
                while (true)
                {
                    try
                    {
                        message = _channel.GetMessage();
                        Process(message);
                    }
                    catch (SerializationException e)
                    {
                        if(e.AgentId.HasValue && e.AgentId.Value>0)
                            Reply(message, new InvalidJsonError() { AgentId = e.AgentId.Value});
                    }
                }
            }
            catch (GameEndedException exc)
            {
                _logger.Log($"GM: GAME WON BY {exc.WinnerTeam}", 1);
            }
        }

        void Process(Message message) //Filter out everything but proper actions
        {
            if (message == null) //Handle nulls
            {
                return;
            }
            if (message is Error e) //Handle errors
            {
                _logger.Log($"GM: Somebody send Error object to the game master... I dont speak with idiots");
                return;
                //Reply(message, new InvalidActionError()
                //{
                //    AgentId = e.AgentId,
                //    Timestamp = GetTimeStamp()
                //});
                //return;
            }
            if (message is GameOverInfo || message is GameStartInfo) //handle infos
            {
                _logger.Log($"GM: Somebody send info object to the game master... I dont speak with idiots");
                return;
            }
            if (message is PlayerJoinResponse || message is GameMasterJoinRequest || message is GameMasterJoinResponse) //handle setup messages
            {
                _logger.Log($"GM: Somebody send wrong type of setup object to the game master... I dont speak with idiots");
                return;
            }
            if (message is Response)
            {
                _logger.Log($"GM: Somebody send Response object to the game master... I dont speak with idiots");
                return;
            }

            if (message is PlayerJoinRequest x)
            {
                TryAddPlayer(x);
                return;
            }
            DoProcess((Action)message);
            OnBoardChanged(new BoardChangedEventArgs(_board, _players));
        }

        private void TryAddPlayer(PlayerJoinRequest newPlayer)
        {
            if (_gameInProgress)
            {
                Reply(newPlayer, new PlayerJoinResponse()
                {
                    AgentId = newPlayer.AgentId,
                    IsConnected = false,
                    Timestamp = GetTimeStamp()
                });
                return; //GAME IN PROGRESS
            }

            if (_players.Any(p => p.AgentId == newPlayer.AgentId))
            {
                Reply(newPlayer, new PlayerJoinResponse()
                {
                    AgentId = newPlayer.AgentId,
                    IsConnected = false,
                    Timestamp = GetTimeStamp()
                });
                return; //SAME IDS
            }

            var ddd = new PlayerClass()
            {
                TeamId = newPlayer.TeamId,
                AgentId = newPlayer.AgentId,
                IsLeader = newPlayer.WantToBeLeader ? !_players.Where(p => p.TeamId == newPlayer.TeamId).Any(p => p.IsLeader) : false,
                Piece = null,
                MyPositionX = -1,
                MyPositionY = -1,
                TimeOfLastRequest = DateTime.MinValue,
                TimePenalty = DateTime.MinValue
            };
            _players.Add(ddd);

            var response = new PlayerJoinResponse()
            {
                AgentId = newPlayer.AgentId,
                IsConnected = true,
                Timestamp = GetTimeStamp()
            };
            Reply(newPlayer, response);
            CheckForGameStart();
        }

        private void DoProcess(Action act)
        {
            var actionHandler = new ActionHandler(_board, _players, GetTimeStamp, _logger);
            var player = _players.Single(p => p.AgentId == act.AgentId);

            if (player.TimePenalty > DateTime.Now && !(act is CommunicationAgreementAction))
            {
                _logger.Log($"GM: Player{player.AgentId} dosent follow the rules! PENALTY!");
                var replyError = new RequestDuringTimePenaltyError()
                {
                    AgentId = act.AgentId,
                    Timestamp = GetTimeStamp(),
                    WaitUntilTime = (int)(player.TimePenalty - _timeZero).TotalMilliseconds + _gameConfiguration.Tpm_rulesDisobeyed
                };
                player.TimeOfLastRequest = DateTime.Now;
                player.TimePenalty = _timeZero.AddMilliseconds(replyError.WaitUntilTime);
                Reply(act, replyError);
                return;
            }

            if (HasPendingRequestFromLeader(act, player))
            {
                Reply(act, new InvalidActionError()
                {
                    AgentId = act.AgentId
                });
                return;//HasPendingRequestFromLeader
            }

            Message response = null;
            try
            {
                switch (act)
                {
                    case CheckPieceAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case CommunicationAgreementAction action:
                        response = actionHandler.Handle(player, action);
                        if (response is CommunicationResponse rr && rr.Agreement)
                        {
                            var temp = TryDequeueDataExchangeRequest(rr.WithAgentId, rr.AgentId);
                            var resp2 = new CommunicationResponse()
                            {
                                AgentId = rr.WithAgentId,
                                Agreement = true,
                                WithAgentId = rr.AgentId,
                                Data = temp.Data,
                                Timestamp = GetTimeStamp(),
                                WaitUntilTime = GetTimeStamp() + _gameConfiguration.Tpm_infoExchange
                            };
                            var player2 = _players.Single(p => p.AgentId == rr.WithAgentId);
                            player2.TimeOfLastRequest = DateTime.Now;
                            player2.TimePenalty = _timeZero.AddMilliseconds(resp2.WaitUntilTime);
                            Reply(temp, resp2);
                        }
                        break;
                    case CommunicationRequestData action:
                        {
                            response = actionHandler.Handle(player, action);
                            if (!(response is Error))
                            {
                                TryDequeueDataExchangeRequest(action.AgentId, action.WithAgentId);
                                _queuedDataExchanges.Add(action);
                            }
                        }
                        break;
                    case CommunicationRequestForwardData action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case DestroyPieceAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case DiscoveryAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case MakeMoveAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case PickPieceAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    case PutPieceAction action:
                        response = actionHandler.Handle(player, action);
                        break;
                    default:
                        _logger.Log($"GM: Somebody send bad object to the game master... I dont speak with idiots");
                        return;
                }
            }
            catch (GameEndedException exc)
            {
                SignalEndGame(exc.WinnerTeam);
                OnBoardChanged(new BoardChangedEventArgs(_board, _players));
                throw;
            }
            if (response is Response resp)
            {
                player.TimeOfLastRequest = DateTime.Now;
                player.TimePenalty = _timeZero.AddMilliseconds(resp.WaitUntilTime);
            }
            Reply(act, response);
        }

        void Reply(Message originalMessage, Message replyMessage)
        {
            if (replyMessage == null)
                return;
            _channel.SendMessage(replyMessage);
        }

        private void CheckForGameStart()
        {
            if (_players.Count >= _gameConfiguration.PlayersCount)
            {
                SignalStartGame();
                StartSpawningPieces();
            }
        }

        private void StartSpawningPieces()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    if (!_gameInProgress)
                        break;
                    _board.TrySpawnPiece();
                    Thread.Sleep(_gameConfiguration.PieceSpawnDelay);
                }
            });
        }

        private void SignalStartGame()
        {
            _gameInProgress = true;
            _logger.Log($"GM: Starting game!!!!!!11");
            var rand = new Random();

            AssignLeaderIfMissing(Team.Bottom);
            AssignLeaderIfMissing(Team.Upper);
            foreach (var player in _players)
            {
                GameStartInfo gameStart = CreateGameStartObject(rand, player, _players, _gameConfiguration);
                SyncPlayerData(gameStart);
                _logger.Log($"GM: Sending GameStartInfo to Player no.{player.AgentId}");
                _channel.SendMessage(gameStart);
            }
            OnBoardChanged(new BoardChangedEventArgs(_board, _players));
        }

        private void AssignLeaderIfMissing(Team team)
        {
            if (!_players.Where(p => p.TeamId == team).Any(p => p.IsLeader))
            {
                for (int i = _players.Count - 1; i >= 0; i--)
                {
                    if (_players[i].TeamId == team)
                    {
                        _players[i].IsLeader = true;
                        break;
                    }
                }
            }
        }

        private GameStartInfo CreateGameStartObject(Random rand, PlayerClass player, List<PlayerClass> players, GameConfiguration gameConfiguration)
        {
            return new GameStartInfo()
            {
                AgentId = player.AgentId,
                AgentIdsFromTeam = players.Where(p => p.TeamId == player.TeamId).Select(p => p.AgentId).ToArray(),
                TeamLeaderId = players.Where(p => p.TeamId == player.TeamId).OrderBy(p => p.AgentId).Select(p => p.AgentId).FirstOrDefault(),
                Timestamp = GetTimeStamp(),
                BoardSizeX = gameConfiguration.BoardSizeX,
                BoardSizeY = gameConfiguration.BoardSizeY,
                GoalAreaHeight = gameConfiguration.GoalAreaHeight,
                InitialXPosition = rand.Next(gameConfiguration.BoardSizeX),
                InitialYPosition = player.TeamId == Team.Bottom ? 0 : gameConfiguration.BoardSizeY - 1,
                NumberOfGoals = gameConfiguration.NumberOfGoals,
                NumberOfPlayers = players.Count,
                PieceSpawnDelay = gameConfiguration.PieceSpawnDelay,
                MaxNumberOfPiecesOnBoard = gameConfiguration.MaxNumberOfPiecesOnBoard,
                ProbabilityOfBadPiece = gameConfiguration.ProbabilityOfBadPiece,
                BaseTimePenalty = gameConfiguration.BaseTimePenalty,
                Tpm_move = gameConfiguration.Tpm_move,
                Tpm_discoverPieces = gameConfiguration.Tpm_discoverPieces,
                Tpm_pickPiece = gameConfiguration.Tpm_pickPiece,
                Tpm_checkPiece = gameConfiguration.Tpm_checkPiece,
                Tpm_destroyPiece = gameConfiguration.Tpm_destroyPiece,
                Tpm_putPiece = gameConfiguration.Tpm_putPiece,
                Tpm_infoExchange = gameConfiguration.Tpm_infoExchange,
            };
        }

        private void SyncPlayerData(GameStartInfo gameStart)
        {
            var player = _players.Where(p => p.AgentId == gameStart.AgentId).Single();
            player.IsLeader = player.AgentId == gameStart.TeamLeaderId;
            player.MyPositionX = gameStart.InitialXPosition;
            player.MyPositionY = gameStart.InitialYPosition;
            player.Piece = null;
        }

        private void SignalEndGame(Team winnerTeam)
        {
            _gameInProgress = false;

            var info = new GameOverInfo()
            {
                AgentId = -1, //CS should will this
                Timestamp = GetTimeStamp(),
                WinningTeam = winnerTeam
            };
            _logger.Log($"GM: Sending GameOverInfo.");
            _channel.SendMessage(info);
        }

        private int GetTimeStamp()
        {
            return (int)(DateTime.Now - _timeZero).TotalMilliseconds;
        }

        private CommunicationRequestData TryDequeueDataExchangeRequest(int agentIdFrom, int agentIdWith)
        {
            List<CommunicationRequestData> matches = _queuedDataExchanges.Where(crd => crd.AgentId == agentIdFrom && crd.WithAgentId == agentIdWith).ToList();
            if (matches.Count == 0)
            {
                return null;
            }
            else if (matches.Count == 1)
            {
                _queuedDataExchanges.Remove(matches[0]);
                return matches[0];
            }
            else
            {
                throw new Exception($"There is more than one information exchange request queued between player{agentIdFrom} and {agentIdWith}");
            }
        }

        private bool HasPendingRequestFromLeader(Action act, PlayerClass player)
        {
            var leader = _players.Where(p => p.TeamId == player.TeamId).FirstOrDefault(p => p.IsLeader);
            if (leader == null)
                return false;

            List<CommunicationRequestData> matches = _queuedDataExchanges.Where(crd => crd.AgentId == player.AgentId && crd.WithAgentId == leader.AgentId).ToList();
            if (matches.Count == 0)
            {
                return false;
            }
            else if (matches.Count == 1)
            {
                if (act is CommunicationAgreementAction agreement && agreement.WithAgentId == leader.AgentId)
                    return false; // he has but it will be resolved with this request
                else
                    return true;
            }
            else
            {
                throw new Exception($"There is more than one information exchange request queued between player{player.AgentId} and {leader.AgentId}");
            }
        }
    }
}
