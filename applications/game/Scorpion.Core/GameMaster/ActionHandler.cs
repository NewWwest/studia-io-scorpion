﻿using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Models.Messages.Actions.Requests;
using Scorpion.Core.Models.Messages.Actions.Responses;
using Scorpion.Core.Models.Messages.Enums;
using Scorpion.Core.Models.Messages.Errors;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Scorpion.Core.Models.Messages.Info;
using Scorpion.Core.Exceptions;
using Scorpion.Core.Utils;

namespace Scorpion.Core.GameMaster
{
    internal class ActionHandler
    {
        private readonly GameConfiguration _gameConfiguration;
        private readonly IReadOnlyList<PlayerClass> _players;
        private readonly Board _board;
        private readonly Func<int> _timeStamper;
        private readonly ILogger _logger;

        public ActionHandler(Board board, IReadOnlyList<PlayerClass> players, Func<int> timeStamper, ILogger logger)
        {
            _players = players;
            _board = board;
            _timeStamper = timeStamper;
            _logger = logger;
            _gameConfiguration = new GameConfiguration();
        }

        public Message Handle(PlayerClass player, CheckPieceAction action)
        {
            if (player.Piece == null)
            {
                return new InvalidActionError() { AgentId = action.AgentId };
            }
            else
            {
                _logger.Log($"GM: Player{player.AgentId} checked his {player.Piece.Valid.Value} piece", 1);
                return new CheckPieceResponse()
                {
                    AgentId = action.AgentId,
                    IsCorrect = player.Piece.Valid.Value,
                    Timestamp = _timeStamper(),
                    WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_checkPiece
                };
            }
        }

        public Message Handle(PlayerClass player, CommunicationAgreementAction action)
        {
            if (_players.Any(p => p.TeamId == player.TeamId && p.AgentId == action.WithAgentId))
            {
                return new InvalidActionError()
                {
                    AgentId = action.AgentId
                };
            }

            return new CommunicationResponse()
            {
                AgentId = action.WithAgentId,
                Agreement = action.Agreement,
                Data = action.Data,
                WithAgentId = action.WithAgentId,
                Timestamp = _timeStamper(),
                WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_infoExchange
            };
        }

        public Message Handle(PlayerClass player, CommunicationRequestData action)
        {
            if (_players.Any(p => p.TeamId == player.TeamId && p.AgentId == action.WithAgentId))
            {
                return new InvalidActionError()
                {
                    AgentId = action.AgentId
                };
            }

            return new CommunicationRequestForwardData()
            {
                AgentId = action.WithAgentId,
                WithAgentId = action.AgentId,
                Timestamp = _timeStamper()
            };
        }

        public Message Handle(PlayerClass player, CommunicationRequestForwardData action)
        {
            _logger.Log($"GM: Somebody send CommunicationRequestForwardData object to the game master... I dont speak with idiots");
            return null;
        }

        public Message Handle(PlayerClass player, DestroyPieceAction action)
        {
            if (player.Piece == null)
            {
                return new InvalidActionError() { AgentId = action.AgentId};
            }
            else
            {
                _logger.Log($"GM: Player{player.AgentId} destroyed his piece", 1);
                player.Piece = null;
                _board.DestoyPiece();
                return new DestroyPieceResponse()
                {
                    AgentId = action.AgentId,
                    Timestamp = _timeStamper(),
                    WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_destroyPiece
                };
            }
        }

        public Message Handle(PlayerClass player, DiscoveryAction action)
        {
            _logger.Log($"GM: Player{player.AgentId} did discovery", 1);
            return new DiscoveryResponse()
            {
                AgentId = action.AgentId,
                ClosestPieces = _board.Discovery(player.MyPositionX, player.MyPositionY),
                Timestamp = _timeStamper(),
                WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_discoverPieces,
            };
        }

        public Message Handle(PlayerClass player, MakeMoveAction action)
        {
            (int x, int y) newPosition = MoveUtility.Move(player.MyPositionX, player.MyPositionY, action.MoveDirection);
            int ymin, ymax;
            if (player.TeamId == Team.Bottom)
            {
                ymin = 0;
                ymax = _gameConfiguration.GoalAreaHeight + _gameConfiguration.TaskAreaHeight;
            }
            else
            {
                ymin = _gameConfiguration.GoalAreaHeight;
                ymax = _gameConfiguration.BoardSizeY;
            }

            if (0 <= newPosition.x && newPosition.x < _gameConfiguration.BoardSizeX && ymin <= newPosition.y && newPosition.y < ymax)
            {
                if (_players.Any(p => p.MyPositionX == newPosition.x && p.MyPositionY == newPosition.y))
                {
                    return new CantMoveInThisDirectionError()
                    {
                        AgentId = action.AgentId,
                        Timestamp = _timeStamper()
                    };
                }

                if (_board.TryMovePlayer(newPosition.x, newPosition.y, out int distanceToPiece))
                {
                    _logger.Log($"GM: Player{player.AgentId} moved to ({newPosition.x},{newPosition.y})", 1);
                    player.MyPositionX = newPosition.x;
                    player.MyPositionY = newPosition.y;
                    return new MakeMoveResponse()
                    {
                        AgentId = action.AgentId,
                        ClosestPiece = distanceToPiece,
                        Timestamp = _timeStamper(),
                        WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_move,
                        NewXPosition = newPosition.x,
                        NewYPosition = newPosition.y,
                    };
                }
                else
                {
                    return new CantMoveInThisDirectionError()
                    {
                        AgentId = action.AgentId,
                        Timestamp = _timeStamper()
                    };
                }
            }
            else
            {
                return new CantMoveInThisDirectionError()
                {
                    AgentId = action.AgentId,
                    Timestamp = _timeStamper()
                };
            }
        }

        public Message Handle(PlayerClass player, PickPieceAction action)
        {
            //Has a piece
            if (player.Piece != null)
            {
                return new InvalidActionError()
                {
                    AgentId = action.AgentId
                };
            }

            if (_board.TryPickUpPiece(player.MyPositionX, player.MyPositionY, out Piece piece)) //stands on a pice
            {
                _logger.Log($"GM: Player{player.AgentId} picked up a piece", 1);
                player.Piece = piece;
                return new PickPieceResponse()
                {
                    AgentId = action.AgentId,
                    Timestamp = _timeStamper(),
                    WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_pickPiece
                };
            }
            else //doesnt stand on a pice
            {
                return new InvalidActionError()
                {
                    AgentId = action.AgentId
                };
            }

        }

        public Message Handle(PlayerClass player, PutPieceAction action)
        {
            if (player.Piece == null)
            {
                return new InvalidActionError()
                {
                    AgentId = player.AgentId
                };
            }

            if (_board.TryPutPiece(player.MyPositionX, player.MyPositionY, player.Piece, out PiecePlacementResult result))
            {
                _logger.Log($"GM: Player{player.AgentId} placed a piece with {result} result", 1);
                player.Piece = null;
                if (result == PiecePlacementResult.GoalCompleted)
                {
                    var winnerTeam = _board.CheckForWinners();
                    if (winnerTeam != null)
                        throw new GameEndedException(winnerTeam.Value);
                }
                return new PutPieceResponse()
                {
                    AgentId = action.AgentId,
                    Result = result,
                    Timestamp = _timeStamper(),
                    WaitUntilTime = _timeStamper() + _gameConfiguration.Tpm_putPiece
                };
            }
            else
            {
                return new InvalidActionError()
                {
                    AgentId = player.AgentId
                };
            }

        }
    }
}
