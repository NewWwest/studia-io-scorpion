﻿using Scorpion.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.GameMaster
{
    //Player model for game master containing only its data,
    //implementation of a player with all of its actions should somewhere else
    public class PlayerClass
    {
        public int AgentId { get; set; }
        public bool IsLeader { get; set; }
        public int MyPositionX { get; set; }
        public int MyPositionY { get; set; }
        public Team TeamId { get; set; }
        public DateTime TimeOfLastRequest { get; set; }
        public DateTime TimePenalty { get; set; }
        public Piece Piece { get; set; }
    }
}
