﻿using Scorpion.Core.Models.Messages.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Utils
{
    static public class MoveUtility
    {
        public static (int x, int y) Move(int oldX, int oldY, Direction direction)
        {
            switch (direction)
            {
                case Direction.Down:
                    return (oldX, oldY - 1);
                case Direction.Up:
                    return (oldX, oldY + 1);
                case Direction.Left:
                    return (oldX - 1, oldY);
                case Direction.Right:
                    return (oldX + 1, oldY);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }
        }

        public static Direction GetOppositeDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Down:
                    return Direction.Up;
                case Direction.Up:
                    return Direction.Down;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
