﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Utils
{
    public static class ArrayUtility
    {
        public static IEnumerable<T> CentralizeArray<T>(int radius, int centerX, int centerY, T[,] array) where T : class
        {
            for (int r = 0; r < radius; r += 2)
            {
                for (int i = 0; i < r + 1; i++)
                {
                    int newX = centerX + i - r / 2;
                    int newY = centerY - r / 2;

                    if (newX >= 0 && newX < array.GetLength(0) && newY >= 0 && newY < array.GetLength(1))
                        yield return array[newX, newY];
                    else
                        yield return null;
                }

                for (int i = 1; i < r; i++)
                {
                    int newX1 = centerX - r / 2;
                    int newX2 = centerX + r / 2;
                    int newY = centerY + i - r / 2;

                    if (newX1 >= 0 && newX1 < array.GetLength(0) && newY >= 0 && newY < array.GetLength(1))
                        yield return array[newX1, newY];
                    else
                        yield return null;

                    if (newX2 >= 0 && newX2 < array.GetLength(0) && newY >= 0 && newY < array.GetLength(1))
                        yield return array[newX2, newY];
                    else
                        yield return null;
                }

                for (int i = 0; i < r + 1; i++)
                {
                    int newX = centerX + i - r / 2;
                    int newY = centerY + r / 2;

                    if (newX >= 0 && newX < array.GetLength(0) && newY >= 0 && newY < array.GetLength(1))
                        yield return array[newX, newY];
                    else
                        yield return null;
                }
            }
        }

        public static IEnumerable<T> ElementsInRing<T>(int radius, int originX, int originY,int cutYarea,  T[,] array)
        {
            int sizeX = array.GetLength(0);
            int sizeY = array.GetLength(1);
            int from = (originX - radius < 0) ? 0 : originX - radius;
            int to = (originX + radius >= sizeX) ? sizeX - 1 : originX + radius;

            for (int x = from; x <= to; x++)
            {
                if (originY + radius < sizeY - cutYarea)
                {
                    yield return array[x, originY + radius];
                }
                if (originY - radius >= cutYarea)
                {
                    yield return array[x, originY - radius];
                }
            }

            from = (originY - radius + 1 < 0) ? 0 : originY - radius + 1;
            to = (originY + radius - 1 >= sizeY) ? sizeY - 1 : originY + radius - 1;
            for (int y = from; y <= to; y++)
            {
                if (originX + radius < sizeX)
                {
                    yield return array[originX + radius, y];
                }

                if (originX - radius > 0)
                {
                    yield return array[originX - radius, y];
                }
            }
        }
    }
}
