﻿using Scorpion.Core.Models.Messages;
using Scorpion.Core.Networking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Dev
{
    public class DevChannel : IChannel
    {
        private readonly DevChannelManager _channelManager;
        private readonly int _agentId;

        public DevChannel(int agentId, DevChannelManager channelManager)
        {
            _channelManager = channelManager;
            _agentId = agentId;
        }
        public void SendMessage(Message requestMessage)
        {
            _channelManager.EnqueueMessage(_agentId, requestMessage);
        }

        public Message GetMessage()
        {
            return _channelManager.GetMessage(_agentId);
        }
    }
}
