﻿using Scorpion.Core.GameMaster;
using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Networking;
using Scorpion.Core.Player;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scorpion.Core.Dev
{
    public class GameRunner
    {
        public void RunGame(string host, int port, int playerCount, EventHandler onChange = null)
        {
            Task.Run(() => RunGM(host, port, onChange));
            Thread.Sleep(1000);
            for (int i = 0; i < playerCount; i++)
            {
                Task.Run(() => RunPlayer(host, port, (Team)(i % 2)));
                Thread.Sleep(1000);
            }
        }


        public void RunGM(string host, int port, EventHandler onChange = null)
        {
            var serializer = new SimpleJsonSerializer();
            var logger = new ConsoleLogger();
            var channel = new TcpChannel(host, port, logger, serializer);
            var gameMasterAgent = new GameMasterAgent(channel, logger);
            if (onChange != null)
                gameMasterAgent.BoardChanged += onChange;
            gameMasterAgent.Run();
        }

        public void RunPlayer(string host, int port, Team teamId)
        {
            var serializer = new SimpleJsonSerializer();
            var logger = new ConsoleLogger();
            var channel = new TcpChannel(host, port, logger, serializer);
            var player = new RandomlyMovingPlayer(teamId, channel, logger);
            player.Run();
        }
    }
}
