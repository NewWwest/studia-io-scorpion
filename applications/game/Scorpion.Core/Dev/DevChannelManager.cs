﻿using Scorpion.Core.Logging;
using Scorpion.Core.Models.Messages;
using Scorpion.Core.Networking;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Dev
{
    public class DevChannelManager
    {
        private readonly ILogger _logger;
        ConcurrentDictionary<int, ConcurrentQueue<Message>> _agentQueues;
        ConcurrentQueue<Message> _gameMasterQueue;
        private object _getMessageLock = new object();
        private object _postMessageLock = new object();


        public DevChannelManager(ILogger logger)
        {
            _logger = logger;
            _agentQueues = new ConcurrentDictionary<int, ConcurrentQueue<Message>>();
        }

        public IChannel GetChannel(int agentId)
        {
            if (agentId < 0)
            {
                _gameMasterQueue = new ConcurrentQueue<Message>();
                var channel = new DevChannel(agentId, this);
                return channel;
            }
            else
            {
                _agentQueues.AddOrUpdate(agentId, new ConcurrentQueue<Message>(), (id, queue) => throw new Exception());
                var channel = new DevChannel(agentId, this);
                return channel;
            }
        }

        internal Message GetMessage(int agentId)
        {
            lock (_getMessageLock)
            {
                if (agentId < 0)
                {
                    _gameMasterQueue.TryDequeue(out Message result);
                    return result;
                }
                else
                {
                    _agentQueues[agentId].TryDequeue(out Message result);
                    return result;
                }
            }
        }

        internal void EnqueueMessage(int agentId, Message requestMessage)
        {
            lock (_postMessageLock)
            {

                if (agentId > 0) //Message to Gm
                {
                    _logger.Log($"CS: Enqueueing message from A{agentId} to GM");
                    _gameMasterQueue.Enqueue(requestMessage);
                }
                else //Message from GM
                {
                    try
                    {
                        var receiverId = (int)((dynamic)requestMessage).AgentId; //THIS HURTS
                        _logger.Log($"CS: Enqueueing message from GM to A{receiverId}");
                        _agentQueues[receiverId].Enqueue(requestMessage);
                    }
                    catch
                    {
                        _logger.Log("CS: Sending message failed - maybe no recipient was specified");
#if DEBUG
                        throw;
#endif
                    }
                }
            }
        }
    }
}
