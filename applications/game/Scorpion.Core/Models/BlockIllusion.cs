﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models
{
    public class BlockIllusion : Block
    {
        public BlockIllusion(int x, int y) : base(x, y)
        {
        }

        public int? DistanceForAgent { get; set; }
    }
}
