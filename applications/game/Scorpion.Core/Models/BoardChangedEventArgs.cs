﻿using Scorpion.Core.GameMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scorpion.Core.Models
{
    public class BoardChangedEventArgs : EventArgs
    {
        public Board Board { get; set; }
        public IEnumerable<PlayerClass> Players { get; set; }
        public BoardChangedEventArgs(Board board, IEnumerable<PlayerClass> players)
        {
            Board = board;
            Players = players.ToList();
        }
    }
}
