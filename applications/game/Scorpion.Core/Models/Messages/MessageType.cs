﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages
{
    public enum MessageType
    {
        //Errors
        AgentNotRespondingError = 2,
        CantMoveInThisDirectionError = 5,
        GameMasterNotConnectedYetError = 6,
        GameMasterNotRespondingError = 1,
        InvalidActionError = 7,
        InvalidJsonError = 0,
        RequestDuringTimePenaltyError = 4,
        WybranoCalyPrzedzialDlaPsaError = 3,
        //Info
        GameOverInfo = 33,
        GameStartInfo = 32,
        //Setup
        GameMasterJoinRequest = 50,
        GameMasterJoinResponse = 51,
        PlayerJoinRequest=48,
        PlayerJoinResponse=49,
        //Actions
        CheckPieceAction = 67,
        CommunicationAgreementAction=72,
        CommunicationRequestData=70,
        CommunicationRequestForwardData=71,
        DestroyPieceAction=68,
        DiscoveryAction=65,
        MakeMoveAction=64,
        PickPieceAction=66,
        PutPieceAction=69, //( ͡° ͜ʖ ͡°)
        //Responses
        CheckPieceResponse=131,
        CommunicationResponse=134,
        DestroyPieceResponse=132,
        DiscoveryResponse=129,
        MakeMoveResponse=128,
        PickPieceResponse=130,
        PutPieceResponse=133,


    }
}
