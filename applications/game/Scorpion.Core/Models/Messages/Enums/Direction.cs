﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Enums
{
    public enum Direction
    {
        // 0- gora , 1- prawo , 2- dol , 3- lewo
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    }
}
