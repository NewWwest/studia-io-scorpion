﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Enums
{
    public enum PiecePlacementResult
    {
        BackInTaskArea = 0,
        GoalCompleted = 1,
        GoalNotCompleted = 2,
        PieceWasFake = 3
        // 0 - piece w task area
        // 1 - piece w goal area ; zrealizowano cel
        // 2 - piece w goal area ; niezrealizowano celu
        // 3 - piece w goal area ; piece byl falszywy
    }
}
