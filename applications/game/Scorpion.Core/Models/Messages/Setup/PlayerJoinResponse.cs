﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Setup
{
    public class PlayerJoinResponse :Message
    {
        public override MessageType MsgId => MessageType.PlayerJoinResponse;
        public int AgentId { get; set; }
        public int Timestamp { get; set; }
        public bool IsConnected { get; set; }

        internal override void Validate()
        {
        }
    }
}
