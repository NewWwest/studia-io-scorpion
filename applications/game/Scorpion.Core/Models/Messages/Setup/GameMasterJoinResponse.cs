﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Setup
{
    public class GameMasterJoinResponse : Message
    {
        public override MessageType MsgId => MessageType.GameMasterJoinResponse;
        public bool IsConnected { get; set; }

        internal override void Validate()
        {
        }
    }
}
