﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Scorpion.Core.Models.Messages.Setup
{
    public class PlayerJoinRequest : Message
    {
        public override MessageType MsgId => MessageType.PlayerJoinRequest;
        public Team TeamId { get; set; }
        public int AgentId { get; set; }
        public bool WantToBeLeader { get; set; }

        internal override void Validate()
        {
            if (!Enum.IsDefined(typeof(Team), TeamId))
                throw new SerializationException($"TeamID value was out fof enum range: {(int)TeamId}");
        }
    }
}
