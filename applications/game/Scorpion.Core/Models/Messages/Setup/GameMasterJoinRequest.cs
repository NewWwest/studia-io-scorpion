﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Setup
{
    public class GameMasterJoinRequest : Message
    {
        public override MessageType MsgId => MessageType.GameMasterJoinRequest;

        internal override void Validate()
        {
        }
    }
}
