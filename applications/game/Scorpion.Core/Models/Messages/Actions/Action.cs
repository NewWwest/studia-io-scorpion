﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions
{
    public abstract class Action : Message
    {
        public int AgentId { get; set; }
    }
}
