﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class DiscoveryResponse : Response
    {
        public override MessageType MsgId => MessageType.DiscoveryResponse;

        public IList<(int x, int y, int dist)> ClosestPieces;

        internal override void Validate()
        {
        }
    }
}
