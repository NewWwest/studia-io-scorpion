﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class DestroyPieceResponse : Response
    {
        public override MessageType MsgId => MessageType.DestroyPieceResponse;

        internal override void Validate()
        {
        }
    }
}
