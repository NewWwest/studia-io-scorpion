﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class PickPieceResponse : Response
    {
        public override MessageType MsgId => MessageType.PickPieceResponse;

        internal override void Validate()
        {
        }
    }
}
