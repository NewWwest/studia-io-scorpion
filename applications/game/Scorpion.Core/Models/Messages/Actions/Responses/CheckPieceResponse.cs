﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class CheckPieceResponse : Response
    {
        public override MessageType MsgId => MessageType.CheckPieceResponse;
        public bool IsCorrect { get; set; }

        internal override void Validate()
        {
        }
    }
}
