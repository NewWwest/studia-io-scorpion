﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class MakeMoveResponse : Response
    {
        public override MessageType MsgId => MessageType.MakeMoveResponse;

        public int ClosestPiece { get; set; }

        public int NewXPosition { get; set; }
        public int NewYPosition { get; set; }

        internal override void Validate()
        {
        }
    }
}
