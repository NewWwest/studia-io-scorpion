﻿using Scorpion.Core.Exceptions;
using Scorpion.Core.Models.Messages.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class PutPieceResponse : Response
    {
        public override MessageType MsgId => MessageType.PutPieceResponse;
        public PiecePlacementResult Result { get; set; }


        internal override void Validate()
        {
            if (!Enum.IsDefined(typeof(PiecePlacementResult), Result))
                throw new SerializationException(AgentId, $"TeamID value was out fof enum range: {(int)Result}");
        }
    }
}
