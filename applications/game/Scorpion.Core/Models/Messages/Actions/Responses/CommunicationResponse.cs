﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public class CommunicationResponse : Response
    {
/*
Message type: Action
Description: Action communication response with data
{
" msgId ": 134,
" agentId ": (int ),
" withAgentId ": (int),
" agreement ": ( bool ),
" timestamp ": (int),
" waitUntilTime ": ( int),
" data ": ( string ) // empty if agreement is false
}
*/
        public override MessageType MsgId => MessageType.CommunicationResponse;
        public int WithAgentId { get; set; }
        public bool Agreement { get; set; }
        public string Data { get; set; }

        internal override void Validate()
        {
        }
    }
}
