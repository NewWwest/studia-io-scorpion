﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Responses
{
    public abstract class Response : Action
    {
        public int Timestamp { get; set; }
        public int WaitUntilTime { get; set; }
    }
}
