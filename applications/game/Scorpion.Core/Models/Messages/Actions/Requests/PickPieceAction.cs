﻿using Scorpion.Core.Models.Messages.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class PickPieceAction : Action
    {
        public override MessageType MsgId => MessageType.PickPieceAction;

        internal override void Validate()
        {
        }
    }
}
