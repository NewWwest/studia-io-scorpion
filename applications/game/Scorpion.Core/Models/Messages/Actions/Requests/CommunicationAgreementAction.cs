﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class CommunicationAgreementAction : Action
    {
/*
Message type: Action
Description: Action communication agreement with data
{
" msgId ": 72,
" agentId ": (int ),
" agreement ": ( bool ),
" withAgentId ": (int),
" data ": ( string ) // empty if agreement is false
}
*/

        public override MessageType MsgId => MessageType.CommunicationAgreementAction;

        public bool Agreement { get; set; }

        public int WithAgentId { get; set; }

        public string Data { get; set; }

        internal override void Validate()
        {
        }
    }
}
