﻿using Scorpion.Core.Exceptions;
using Scorpion.Core.Models.Messages.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class MakeMoveAction : Action
    {
        public override MessageType MsgId => MessageType.MakeMoveAction;
        
        public Direction MoveDirection { get; set; }
        
        internal override void Validate()
        {
            if (!Enum.IsDefined(typeof(Direction), MoveDirection))
                throw new SerializationException(AgentId, $"TeamID value was out fof enum range: {(int)MoveDirection}");
        }
    }
}
