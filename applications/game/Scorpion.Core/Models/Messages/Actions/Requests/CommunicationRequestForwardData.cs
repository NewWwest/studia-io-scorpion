﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class CommunicationRequestForwardData : Action
    {
/*
Message type: Action
Description: Action communication request
Remarks: sent by GM to agent with whom someone wants to exchange data
{
" msgId ": 71,
" agentId ": (int ),
" withAgentId ": (int), // who wants to exchange data
with you
" timestamp ": (int)
}
*/

        public override MessageType MsgId => MessageType.CommunicationRequestForwardData;

        public int WithAgentId { get; set; }

        public int Timestamp { get; set; }

        internal override void Validate()
        {
        }
    }
}
