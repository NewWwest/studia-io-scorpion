﻿using Scorpion.Core.Models.Messages.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class CheckPieceAction : Action
    {
        public override MessageType MsgId => MessageType.CheckPieceAction;

        internal override void Validate()
        {
        }
    }
}
