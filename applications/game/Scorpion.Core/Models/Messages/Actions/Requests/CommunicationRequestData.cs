﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Actions.Requests
{
    public class CommunicationRequestData : Action
    {
/*
 * Message type: Action
Description: Action communication request with data
Remarks: sent by agent who wants to exchange data
{
" msgId ": 70,
" agentId ": (int ),
" withAgentId ": (int),
" data ": ( string )
}
*/

        public override MessageType MsgId => MessageType.CommunicationRequestData;

        public int WithAgentId { get; set; }

        public string Data { get; set; }

        internal override void Validate()
        {
        }
    }
}
