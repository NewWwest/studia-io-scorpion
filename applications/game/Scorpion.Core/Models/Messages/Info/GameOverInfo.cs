﻿using Scorpion.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Info
{
    class GameOverInfo : Message
    {
        public override MessageType MsgId => MessageType.GameOverInfo;
        public int AgentId { get; set; }
        public int Timestamp { get; set; }
        public Team WinningTeam { get; set; }

        internal override void Validate()
        {
            if (!Enum.IsDefined(typeof(Team), WinningTeam))
                throw new SerializationException(AgentId, $"TeamID value was out fof enum range: {(int)WinningTeam}");
        }
    }
}
