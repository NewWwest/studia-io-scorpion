﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Info
{
    public class GameStartInfo : Message
    {
        public override MessageType MsgId => MessageType.GameStartInfo;
        public int AgentId { get; set; }
        public int[] AgentIdsFromTeam { get; set; }
        public int TeamLeaderId { get; set; }
        public int Timestamp { get; set; }
        public int BoardSizeX { get; set; }
        public int BoardSizeY { get; set; }
        public int GoalAreaHeight { get; set; }
        public int InitialXPosition { get; set; }
        public int InitialYPosition { get; set; }
        public int NumberOfGoals { get; set; }
        public int NumberOfPlayers { get; set; }
        public int PieceSpawnDelay { get; set; }
        public int MaxNumberOfPiecesOnBoard { get; set; }
        public float ProbabilityOfBadPiece { get; set; }
        public int BaseTimePenalty { get; set; }
        public int Tpm_move { get; set; }
        public int Tpm_discoverPieces { get; set; }
        public int Tpm_pickPiece { get; set; }
        public int Tpm_checkPiece { get; set; }
        public int Tpm_destroyPiece { get; set; }
        public int Tpm_putPiece { get; set; }
        public int Tpm_infoExchange { get; set; }

        internal override void Validate()
        {
        }
    }
}
