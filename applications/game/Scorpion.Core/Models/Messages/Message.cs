﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages
{
    public abstract class Message
    {
        public abstract MessageType MsgId { get; }

        internal abstract void Validate();
    }
}
