﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class InvalidActionError : Error
    {
        public override MessageType MsgId => MessageType.InvalidActionError;

        public int AgentId { get; set; }

        internal override void Validate()
        {
        }
    }
}
