﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class CantMoveInThisDirectionError : Error
    {
        public override MessageType MsgId => MessageType.CantMoveInThisDirectionError;
        
        public int AgentId { get; set; }

        public int Timestamp { get; set; }

        internal override void Validate()
        {
        }
    }
}
