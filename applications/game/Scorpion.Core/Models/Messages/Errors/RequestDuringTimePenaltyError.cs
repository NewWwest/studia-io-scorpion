﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class RequestDuringTimePenaltyError : Error
    {
        public override MessageType MsgId => MessageType.RequestDuringTimePenaltyError;

        public int WaitUntilTime { get; set; }

        public int Timestamp { get; set; }

        public int AgentId { get; set; }

        internal override void Validate()
        {
        }
    }
}
