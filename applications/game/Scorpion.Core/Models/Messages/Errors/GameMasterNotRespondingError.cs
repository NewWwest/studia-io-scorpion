﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class GameMasterNotRespondingError : Error
    {
        public override MessageType MsgId => MessageType.GameMasterNotRespondingError;

        public int AgentId { get; set; }

        internal override void Validate()
        {
        }
    }
}
