﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class AgentNotRespondingError : Error
    {
        public override MessageType MsgId => MessageType.AgentNotRespondingError;

        public int AgentId { get; set; }

        internal override void Validate()
        {
        }
    }
}
