﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class InvalidJsonError : Error
    {
        public override MessageType MsgId => MessageType.InvalidJsonError;

        public int AgentId { get; set; }

        internal override void Validate()
        {
        }
    }
}
