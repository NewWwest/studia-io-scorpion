﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    public class GameMasterNotConnectedYetError : Error
    {
        public override MessageType MsgId => MessageType.GameMasterNotConnectedYetError;
        
        internal override void Validate()
        {
        }
    }
}
