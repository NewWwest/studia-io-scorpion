﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models.Messages.Errors
{
    [Obsolete]
    public class WybranoCalyPrzedzialDlaPsaError : Error
    {
        public override MessageType MsgId => MessageType.WybranoCalyPrzedzialDlaPsaError;

        internal override void Validate()
        {
        }
    }
}
