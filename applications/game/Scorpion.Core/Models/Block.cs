﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Models
{
    public class Block
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Block(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Piece Piece { get; set; }

        public BlockState State { get; set; }
    }

    public enum BlockState
    {
        Default=0,
        Goal = 1,
        DiscoveredGoal = 2,
        DiscoveredNonGoal = 3
    }
}
