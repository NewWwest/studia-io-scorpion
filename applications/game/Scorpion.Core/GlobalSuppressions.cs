﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0042:Deconstruct variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:Scorpion.Core.GameMaster.ActionHandler.Handle(Scorpion.Core.GameMaster.PlayerClass,Scorpion.Core.Models.Messages.Actions.Requests.MakeMoveAction)~Scorpion.Core.Models.Messages.Message")]

