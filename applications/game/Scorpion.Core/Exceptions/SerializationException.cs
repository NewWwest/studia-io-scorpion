﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Exceptions
{
    public class SerializationException : Exception
    {
        public int? AgentId { get; set; }

        public SerializationException(string message) : base(message)
        {
        }

        public SerializationException(string message, Exception innerException) : base(message, innerException)
        {
        }
        public SerializationException(int agentId, string message) : base(message)
        {
            AgentId = agentId;
        }

        public SerializationException(int agentId, string message, Exception innerException) : base(message, innerException)
        {
            AgentId = agentId;
        }
    }
}
