﻿using Scorpion.Core.Models.Messages.Errors;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Exceptions
{
    public class PlayerWasRefusedConnectionException : Exception
    {
        public Error Error { get; set; }

        public PlayerWasRefusedConnectionException(Error error)
        {
            Error = error;
        }
        public PlayerWasRefusedConnectionException()
        {

        }
    }
}
