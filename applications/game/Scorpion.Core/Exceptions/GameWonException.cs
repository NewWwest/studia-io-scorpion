﻿using Scorpion.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core.Exceptions
{
    //pythonic aproach
    public class GameEndedException : Exception
    {
        public Team WinnerTeam { get; set; }
        public GameEndedException(Team winnerTeam)
        {
            WinnerTeam = winnerTeam;
        }
    }
}
