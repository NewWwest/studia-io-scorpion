﻿using Microsoft.Extensions.Configuration;

namespace Scorpion.Core
{
    public class TechnicalConfiguration
    {
        private readonly IConfigurationRoot _configuration;

        public TechnicalConfiguration()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("settings.json");
            _configuration = builder.Build();
        }

        public int SleepTime => int.Parse(_configuration["TechnicalConfiguration:SleepTime"]);
        public int GameMasterSleepTime => int.Parse(_configuration["TechnicalConfiguration:GameMasterSleepTime"]);

        public string Host => _configuration["TechnicalConfiguration:Host"];
        public int Port => int.Parse(_configuration["TechnicalConfiguration:Port"]);
    }
}
