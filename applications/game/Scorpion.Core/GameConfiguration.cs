﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scorpion.Core
{
    public class GameConfiguration
    {
        private readonly IConfigurationRoot _configuration;

        public GameConfiguration()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("settings.json");
            _configuration = builder.Build();
        }

        //gameconfig
        public int PlayersCount => int.Parse(_configuration["GameConfiguration:PlayersCount"]);

        public int MaxPieceNumber => int.Parse(_configuration["GameConfiguration:MaxPieceNumber"]);
        public int ValidPiecePercentage => int.Parse(_configuration["GameConfiguration:ValidPiecePercentage"]);
        public int PieceSpawnDelay => int.Parse(_configuration["GameConfiguration:PieceSpawnDelay"]);
        public int InitialPieces => int.Parse(_configuration["GameConfiguration:InitialPieces"]);
        public int NumberOfGoals => int.Parse(_configuration["GameConfiguration:NumberOfGoals"]);
        public int GoalAreaHeight => int.Parse(_configuration["GameConfiguration:GoalAreaHeight"]);
        public int TaskAreaHeight => int.Parse(_configuration["GameConfiguration:TaskAreaHeight"]);
        public int BoardSizeX => int.Parse(_configuration["GameConfiguration:BoardSizeX"]);

        public int BoardSizeY => TaskAreaHeight + 2 * GoalAreaHeight;
        public int MaxNumberOfPiecesOnBoard => TaskAreaHeight * BoardSizeX;
        public float ProbabilityOfBadPiece => (100 - ValidPiecePercentage) / 100f;

        //time delays
        public int Tpm_rulesDisobeyed => int.Parse(_configuration["GameConfiguration:Tpm_rulesDisobeyed"]);
        public int BaseTimePenalty => int.Parse(_configuration["GameConfiguration:BaseTimePenalty"]);
        public int Tpm_move => int.Parse(_configuration["GameConfiguration:Tpm_move"]);
        public int Tpm_discoverPieces => int.Parse(_configuration["GameConfiguration:Tpm_discoverPieces"]);
        public int Tpm_pickPiece => int.Parse(_configuration["GameConfiguration:Tpm_pickPiece"]);
        public int Tpm_checkPiece => int.Parse(_configuration["GameConfiguration:Tpm_checkPiece"]);
        public int Tpm_destroyPiece => int.Parse(_configuration["GameConfiguration:Tpm_destroyPiece"]);
        public int Tpm_putPiece => int.Parse(_configuration["GameConfiguration:Tpm_putPiece"]);
        public int Tpm_infoExchange => int.Parse(_configuration["GameConfiguration:Tpm_infoExchange"]);
    }
}
