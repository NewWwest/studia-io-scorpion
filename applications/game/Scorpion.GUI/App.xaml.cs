using Avalonia;
using Avalonia.Markup.Xaml;

namespace Scorpion.GUI
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
   }
}