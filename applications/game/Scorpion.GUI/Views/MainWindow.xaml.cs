using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.Threading;
using Scorpion.Core.GameMaster;
using Scorpion.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Scorpion.GUI.Views
{
    public class MainWindow : Window
    {
        private bool boardCreated = false;
        private Grid grid;

        private List<(PlayerClass player, Ellipse ellipse)> circles = new List<(PlayerClass, Ellipse)>();
        private List<(Block block, Ellipse ellipse)> pieces = new List<(Block block, Ellipse ellipse)>();
        private List<(Block field, Ellipse ellipse)> goals = new List<(Block field, Ellipse ellipse)>();
        public MainWindow()
        {
            InitializeComponent();
            grid = this.Get<Grid>("grid");
        }
        private void CreateBoard(Board board, IEnumerable<PlayerClass> players)
        {
            for (int i = 0; i < board.GameConfiguration.BoardSizeX; i++)
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            for (int i = 0; i < board.GameConfiguration.BoardSizeY; i++)
                grid.RowDefinitions.Add(new RowDefinition());

            for (int y = 0; y < grid.RowDefinitions.Count; y++)
                for (int x = 0; x < grid.ColumnDefinitions.Count; x++)
                {
                    var cell = new Rectangle();

                    if (y < board.GameConfiguration.GoalAreaHeight)
                    {
                        cell.Fill = Brushes.LightPink;
                    }
                    else if (y < board.GameConfiguration.BoardSizeY - board.GameConfiguration.GoalAreaHeight)
                    {
                        cell.Fill = Brushes.Beige;
                    }
                    else
                    {
                        cell.Fill = Brushes.PaleTurquoise;
                    }

                    var border = new Border()
                    {
                        BorderThickness = new Thickness(1),
                        Background = Brushes.Violet,
                    };

                    border.Child = cell;

                    border[!WidthProperty] = grid[!WidthProperty];
                    border[!HeightProperty] = grid[!HeightProperty];
                    SetElementInGrid(border, x, y);
                }

           
        }
        private void MoveElement(Control control, int x, int y)
        {
            Grid.SetRow(control, grid.RowDefinitions.Count - 1 - y);
            Grid.SetColumn(control, x);
        }
        private void SetElementInGrid(Control control, int x, int y)
        {
            MoveElement(control, x, y);
            grid.Children.Add(control);
        }

        public void ChangeBoard(Board board, IEnumerable<PlayerClass> players)
        {
            Dispatcher.UIThread.InvokeAsync(() =>
            {
                if (!boardCreated)
                {
                    CreateBoard(board, players);
                    DisplayPlayers(players);
                    DisplayPieces(board);
                    DisplayGoals(board);
                    boardCreated = true;
                }
                UpdatePieces(board);
                UpdateGoals();
                UpdatePlayers(players);
            });
        }

        private void UpdatePlayers(IEnumerable<PlayerClass> players)
        {
            foreach (var circle in circles)
            {
                MoveElement(circle.ellipse, circle.player.MyPositionX, circle.player.MyPositionY);
            }
        }

        private void UpdateGoals()
        {
            foreach(var goal in goals)
            {
                switch (goal.field.State)
                {
                    case BlockState.DiscoveredGoal:
                        goal.ellipse.Fill = Brushes.Green;
                        break;
                    case BlockState.DiscoveredNonGoal:
                        goal.ellipse.Fill = Brushes.Red;
                        break;
                    case BlockState.Goal:
                        goal.ellipse.Fill = Brushes.LightGray;
                        break;
                }
            }

        }

        private void UpdatePieces(Board board)
        {
            foreach(var piece in pieces)
            {
                if (piece.block.Piece == null)
                {
                    grid.Children.Remove(piece.ellipse);
                    //pieces.Remove(piece);
                }
            }
        }

        private void DisplayPlayers(IEnumerable<PlayerClass> players)
        {
            foreach (var player in players)
            {
                int x = player.MyPositionX;
                int y = player.MyPositionY;

                var diameter = 30;

                Ellipse circle = new Ellipse()
                {
                    Height = diameter,
                    Width = diameter,
                    Fill = player.TeamId == Team.Upper ? Brushes.Blue : Brushes.Red,
                    VerticalAlignment = Avalonia.Layout.VerticalAlignment.Center,
                    HorizontalAlignment = Avalonia.Layout.HorizontalAlignment.Center,
                };
                SetElementInGrid(circle, x, y);


                circles.Add((player, circle));
            }
        }

        private void DisplayGoals(Board board)
        {
            for (int y = 0; y < grid.RowDefinitions.Count; y++)
                for (int x = 0; x < grid.ColumnDefinitions.Count; x++)
                {
                    ISolidColorBrush brush = null;

                    switch (board.Blocks[x, y].State)
                    {
                        case Core.Models.BlockState.Default:
                            brush = Brushes.Transparent;
                            break;
                        case Core.Models.BlockState.DiscoveredGoal:
                            brush = Brushes.LightYellow;
                            break;
                        case Core.Models.BlockState.DiscoveredNonGoal:
                            brush = Brushes.LightGray;
                            break;
                        case Core.Models.BlockState.Goal:
                            brush = Brushes.LightGreen;
                            break;
                    }

                    if (brush == null)
                        continue;

                    var diameter = 10;

                    Ellipse circle = new Ellipse()
                    {
                        Height = diameter,
                        Width = diameter,
                        Fill = brush,
                        VerticalAlignment = Avalonia.Layout.VerticalAlignment.Center,
                        HorizontalAlignment = Avalonia.Layout.HorizontalAlignment.Center,
                    };
                    SetElementInGrid(circle, x, y);
                    goals.Add((board.Blocks[x, y], circle));
                }
        }

        private void DisplayPieces(Board board)
        {
            for (int y = 0; y < grid.RowDefinitions.Count; y++)
                for (int x = 0; x < grid.ColumnDefinitions.Count; x++)
                {
                    ISolidColorBrush brush = null;

                    if (board.Blocks[x, y].Piece != null)
                    {
                        if (board.Blocks[x, y].Piece.Valid == true)
                            brush = Brushes.Yellow;
                        else if (board.Blocks[x, y].Piece.Valid == false)
                            brush = Brushes.Brown;
                        else
                            brush = Brushes.LightPink;
                    }
                    if (brush == null)
                        continue;

                    var diameter = 10;

                    Ellipse circle = new Ellipse()
                    {
                        Height = diameter,
                        Width = diameter,
                        Fill = brush,
                        VerticalAlignment = Avalonia.Layout.VerticalAlignment.Center,
                        HorizontalAlignment = Avalonia.Layout.HorizontalAlignment.Center,
                    };
                    SetElementInGrid(circle, x, y);
                    pieces.Add((board.Blocks[x, y], circle));
                }
        }

        private Rectangle GetChildren(int x, int y)
        {
            var border = grid.Children.Cast<Border>().First(e => Grid.GetRow(e) == grid.RowDefinitions.Count - 1 - y && Grid.GetColumn(e) == x);
            return (Rectangle)border.Child;
        }

        private ISolidColorBrush PickRandomBrush()
        {
            ISolidColorBrush result = Brushes.Transparent;
            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (ISolidColorBrush)properties[random].GetValue(null, null);

            return result;
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}