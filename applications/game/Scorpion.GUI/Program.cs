﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Logging.Serilog;
using Scorpion.Core;
using Scorpion.Core.Dev;
using Scorpion.Core.GameMaster;
using Scorpion.Core.Logging;
using Scorpion.Core.Models;
using Scorpion.Core.Networking;
using Scorpion.Core.Player;
using Scorpion.GUI.ViewModels;
using Scorpion.GUI.Views;

namespace Scorpion.GUI
{
    class Program
    {
        static MainWindow window;
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args) => BuildAvaloniaApp().Start(AppMain, args);

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug()
                .UseReactiveUI();

        // Your application's entry point. Here you can initialize your MVVM framework, DI
        // container, etc.
        private static void AppMain(Application app, string[] args)
        {
            var model = new MainWindowViewModel();

            window = new MainWindow
            {
                DataContext = model
            };

            Task.Run(() => {
                var techConfig = new TechnicalConfiguration();
                var gameConfig = new GameConfiguration();
                var xd = new GameRunner();
                xd.RunGame(techConfig.Host, techConfig.Port, gameConfig.PlayersCount, Gm_BoardChanged);

            });
            app.Run(window);
        }

        //private static void RunGame2(MainWindowViewModel model)
        //{
        //    Task.Run(() => RunGM());
        //    Thread.Sleep(3000);
        //    Task.Run(() => RunPlayer());
        //    Thread.Sleep(3000);
        //    Task.Run(() => RunPlayer2());
        //    Thread.Sleep(3000);

        //    Console.WriteLine("Game is Running. Press ESC to terminate:");
        //    while (Console.ReadKey().Key != ConsoleKey.Escape) { }


        //    void RunGM()
        //    {
        //        var serializer = new SimpleJsonSerializer();
        //        var logger = new ConsoleLogger();
        //        var channel = new TcpChannel("localhost", 1916, logger, serializer);
        //        var gameMasterAgent = new GameMasterAgent(channel, logger);
        //        gameMasterAgent.BoardChanged += Gm_BoardChanged;
        //        gameMasterAgent.Run();
        //    }

        //    void RunPlayer()
        //    {
        //        var serializer = new SimpleJsonSerializer();
        //        var logger = new ConsoleLogger();
        //        var channel = new TcpChannel("localhost", 1916, logger, serializer);
        //        var player = new RandomlyMovingPlayer(1, 1, channel, logger);
        //        player.Run();
        //    }
        //    void RunPlayer2()
        //    {
        //        var serializer = new SimpleJsonSerializer();
        //        var logger = new ConsoleLogger();
        //        var channel = new TcpChannel("localhost", 1916, logger, serializer);
        //        var player = new RandomlyMovingPlayer(2, 2, channel, logger);
        //        player.Run();
        //    }
        //}
        

        private static void Gm_BoardChanged(object sender, EventArgs e)
        {
            BoardChangedEventArgs args = (BoardChangedEventArgs)e;
            window.ChangeBoard(args.Board, args.Players);
        }

        private static PlayerClass PlayerAgentToPlayerClass(PlayerAgent player)
        {
            return new PlayerClass()
            {
                AgentId = player.AgentId,
                Piece = null,
                MyPositionX = 0,
                MyPositionY = player.TeamId == Team.Bottom ? 0 : new GameConfiguration().BoardSizeY - 1, //has to match Game master signalStartGame signal
                TeamId = player.TeamId,
                TimeOfLastRequest = DateTime.Now,
                TimePenalty = DateTime.Now
            };
        }
    }
}
