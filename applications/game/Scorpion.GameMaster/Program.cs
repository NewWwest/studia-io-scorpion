﻿using Scorpion.Core;
using Scorpion.Core.Dev;
using Scorpion.Core.GameMaster;
using Scorpion.Core.Logging;
using Scorpion.Core.Networking;
using System;

namespace Scorpion.GameMaster
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var config = new TechnicalConfiguration();
            var xd = new GameRunner();
            xd.RunGM(config.Host, config.Port);

            Console.ReadLine();
        }
    }
}
