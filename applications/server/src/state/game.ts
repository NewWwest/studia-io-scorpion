export enum GameState {
  WaitingForGameMaster,
  WaitingForGameStart,
  LiveGame,
  EndOfGame,
}

let state = GameState.WaitingForGameMaster;

const setGameState = (stateSetter: (state: GameState) => GameState) => {
  state = stateSetter(state);
}

const getGameState = () => state;

export { setGameState, getGameState };
