import JsonSocket from 'json-socket';

type ClientConnection = {
  socket: JsonSocket;
  agentId: number;
  isConnected: boolean;
}

type Clients = {
  users: ClientConnection[];
  gm: Pick<ClientConnection, "socket" | "isConnected"> | null;
}

let clients: Clients = {
  users: [],
  gm: null,
}

const getClients = () => clients;
const setClients = (clientsSetter: (clients: Clients) => Clients) => {
  clients = clientsSetter(clients);
}
const addUser = (user: ClientConnection) => {
  clients.users.push(user);
};

const acceptUser = (agentId: number) => {
  clients.users = clients.users.map(user => {
    if(user.agentId === agentId) {
      user.isConnected = true;
    }
    return user;
  })
}

export {
  getClients,
  setClients,
  addUser,
  acceptUser,
}
