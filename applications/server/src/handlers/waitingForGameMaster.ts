import JsonSocket from "json-socket";
import { getClients, addUser, acceptUser } from "../state/clients";

type HandlerProps = {
  json: any;
  client: JsonSocket;
}

const checkForGameMaster = ({ json, client }: HandlerProps, onSucces: () => void) => {
  const onError = (error: Error) => {
    if (error) {
      console.error(error);
    }
  }
  switch (json.msgId) {
    case 50:
      client.sendMessage({
        msgId: 51,
        isConnected: true,
      }, onSucces);
      break;
    case 48:
      client.sendMessage({
        msgId: 6,
      }, onError);
      break;
    default:
      const clients = getClients();
      const isUser = clients.users.find(u => u.socket === client );
      let agentId;
      if (!isUser) {
        agentId = clients.users.length + 1;
        addUser({ socket: client, isConnected: false, agentId });
      }
      client.sendMessage({
        msgId: 7,
        agentId, // How can i know what is proper agentId during that stage?
      }, onError)
  }
}

export { checkForGameMaster };
