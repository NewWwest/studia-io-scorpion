import JsonSocket from "json-socket";
import { getClients } from "../state/clients";
import { sendToClient } from "../utils/broadcast";

type HandlerProps = {
  json: any;
  client: JsonSocket;
}

const forwardToGameMaster = (json: any, gameMaster: JsonSocket) => {
  gameMaster.sendMessage(json, () => {
    console.log("Request for access has been sent to Game Master.");
  })
}

const handleGameRequests = ({ json, client }: HandlerProps, onSucces: () => void) => {
  const onError = (error: Error) => {
    if (error) {
      console.error(error);
    }
  }
  const gm = getClients().gm;
  if (client === gm.socket) {
    // Request from GM
    sendToClient({ data: json, agentId: json.agentId });
  } else {
    // Request from player
    forwardToGameMaster(json, gm.socket);
  }
}

export { handleGameRequests };
