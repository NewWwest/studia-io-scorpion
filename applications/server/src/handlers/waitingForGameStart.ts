import JsonSocket from "json-socket";
import { setGameState, GameState } from "../state/game";
import { getClients, addUser, acceptUser } from "../state/clients";
import { sendToClient, sendToClients } from "../utils/broadcast";

type HandlerProps = {
  json: any;
  client: JsonSocket;
}

const forwardToGameMaster = (json: any, gameMaster: JsonSocket) => {
  gameMaster.sendMessage(json, () => {
    console.log("Request for access has been sent to Game Master.");
  })
}

const checkForPlayer = ({ json, client }: HandlerProps, onSucces: () => void) => {
  const onError = (error: Error) => {
    if (error) {
      console.error(error);
    }
  }
  const gm = getClients().gm;
  console.log(getClients().users.length);
  switch (json.msgId) {
    case 48:
      if(json.agentId <= 0) {
        const clients = getClients();
        json.agentId = clients.users.length + 1;
      }
      addUser({ socket: client, isConnected: false, agentId: json.agentId });
      forwardToGameMaster(json, gm.socket);
      break;
    case 49:
      acceptUser(json.agentId);
      sendToClient({ data: json, agentId: json.agentId });
      break;
    case 32:
      sendToClient({ data: json, agentId: json.agentId }, () => {
        setGameState(() => GameState.LiveGame);
      });
      break;
    default:
      client.sendMessage({
        msgId: 7,
        agentId: 0,
      }, onError)
  }
}

export { checkForPlayer };
