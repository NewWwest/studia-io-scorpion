import net from "net";
import JsonSocket from "./utils/json-socket";

import { checkForGameMaster } from "./handlers/waitingForGameMaster";
import { checkForPlayer } from "./handlers/waitingForGameStart";
import { handleGameRequests } from "./handlers/gameRequests";
import { setGameState, getGameState, GameState } from "./state/game";
import { setClients } from "./state/clients"

const server = net.createServer();

server.on('connection', conn => {
  conn.setEncoding('binary');
  console.log(`Opening connection with new client`);
  const client = new JsonSocket(conn);
  
  client.on('message', (json: any) => {
        console.log("Server recieved json: ", json);
        switch (getGameState()) {
          case GameState.WaitingForGameMaster:
              checkForGameMaster({ json, client}, () => {
                setGameState(() => GameState.WaitingForGameStart);
                setClients(clients => ({
                  ...clients,
                  gm: { socket: client, isConnected: true },
                }));
                console.log("Game Master has connected to the server.");
              });
            break;
          case GameState.WaitingForGameStart:
              checkForPlayer({ json, client }, () => {
                console.log(`New Player has connected to the server.`);
              })
            break;
          case GameState.LiveGame:
              handleGameRequests({ json, client }, () => {
                // todo...
              });
            break;
          case GameState.EndOfGame:
            break;
          default:
            break;
        }
  });

  client.on('close', (err) => {
    if (err) {
      console.log(`Client thrown an error: ${err}`);
    }
    console.log(`Closing connection with client`);
    setClients(clients => ({
      ...clients,
      users: clients.users.filter(c => c.socket !== client),
    }))
  })

  client.on('error', () => {
    //
  });
});


server.on('close', () => { 
    console.log(`Server disconnected`);
});

server.on('error', error => { 
    console.log(`Error : ${error}`);
});


server.listen(1916);
console.log("🤖 Server is listening on port 1916 (L)");
