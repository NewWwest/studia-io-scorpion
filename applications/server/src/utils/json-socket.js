var net = require('net');
var StringDecoder = require('string_decoder').StringDecoder;
var decoder = new StringDecoder();

function to4BytesBuffer(size) {
  const buf = Buffer.allocUnsafe(4);
  buf.writeUInt32LE(size, 0)
  buf.reverse();
  return buf
}

var JsonSocket = function(socket, opts) {
    this._socket = socket;
    this._contentLength = null;
    this._buffer = '';
    this._opts = opts || {}
    if (!this._opts.delimeter) {
        this._opts.delimeter = '{';
    }
    this._closed = false;
    socket.on('data', this._onData.bind(this));
    socket.on('connect', this._onConnect.bind(this));
    socket.on('close', this._onClose.bind(this));
    socket.on('err', this._onError.bind(this));
};
module.exports = JsonSocket;

JsonSocket.sendSingleMessage = function(port, host, message, opts, callback) {
    if (typeof(opts) === "function") {
        callback = opts;
        opts = {};
    } else {
        callback = callback || function(){};
        if (!opts) {
            opts = {};
        }
    }
    var socket = new JsonSocket(new net.Socket(), opts);
    socket.connect(port, host);
    socket.on('error', function(err) {
        callback(err);
    });
    socket.on('connect', function() {
        socket.sendEndMessage(message, callback);
    });
};

JsonSocket.sendSingleMessageAndReceive = function(port, host, message, opts, callback) {
    if (typeof(opts) === "function") {
        callback = opts;
        opts = {};
    } else {
        callback = callback || function(){};
        if (!opts) {
            opts = {};
        }
    }
    var socket = new JsonSocket(new net.Socket(), opts);
    socket.connect(port, host);
    socket.on('error', function(err) {
        callback(err);
    });
    socket.on('connect', function() {
        socket.sendMessage(message, function(err) {
            if (err) {
                socket.end();
                return callback(err);
            }
            socket.on('message', function(message) {
                socket.end();
                if (message.success === false) {
                    return callback(new Error(message.error));
                }
                callback(null, message)
            });
        });
    });
};

JsonSocket.prototype = {

    _onData: function(data) {
        data = decoder.write(data);
        try {
            this._handleData(data);
        } catch (e) {
            this.sendError(e);
        }
    },
    _handleData: function(data) {
        this._buffer += data;
        if (this._contentLength == null) {
          var i = 8; // First 8 bytes are content length and msg id
            // Check first 4 bytes which are content length
                if(this._buffer.length < 8) return;
                var rawContentLength = Buffer.from(this._buffer).slice(0, 4);
                this._contentLength = rawContentLength.reverse().readUInt32LE();
                if (isNaN(this._contentLength)) {
                    this._contentLength = null;
                    this._buffer = '';
                    var err = new Error('Invalid content length supplied ('+rawContentLength+') in: '+this._buffer);
                    err.code = 'E_INVALID_CONTENT_LENGTH';
                    throw err;
                }
                this._buffer = this._buffer.substring(i);
        }
        if (this._contentLength != null) {
            var length = Buffer.byteLength(this._buffer);
            if (length == this._contentLength) {
                this._handleMessage(this._buffer);
            } else if (length > this._contentLength) {
                var message = this._buffer.substring(0, this._buffer.indexOf("}") + 1); // BRUTAL HACK, FIX THIS, dotnet sending us wrong length, but it works anyway so fck it
                var rest = this._buffer.substring(this._buffer.indexOf("}") + 1);
                this._handleMessage(message);
                this._onData(rest);
            }
        }
    },
    _handleMessage: function(data) {
        this._contentLength = null;
        this._buffer = '';
        var message;
        try {
            message = JSON.parse(data);
        } catch (e) {
            var err = new Error('Could not parse JSON: '+e.message+'\nRequest data: '+data);
            err.code = 'E_INVALID_JSON';
            throw err;
        }
        message = message || {};
        this._socket.emit('message', message);
    },

    sendError: function(err) {
        this.sendMessage(this._formatError(err));
    },
    sendEndError: function(err) {
        this.sendEndMessage(this._formatError(err));
    },
    _formatError: function(err) {
        return {success: false, error: err.toString()};
    },

    sendMessage: function(message, callback) {
        if (this._closed) {
            if (callback) {
                callback(new Error('The socket is closed.'));
            }
            return;
        }
        this._socket.write(this._formatMessageData(message), 'utf-8', callback);
    },
    sendEndMessage: function(message, callback) {
        var that = this;
        this.sendMessage(message, function(err) {
            that.end();
            if (callback) {
                if (err) return callback(err);
                callback();
            }
        });
    },
    _formatMessageData: function(message) {
        var messageData = JSON.stringify(message);
        var length = Buffer.byteLength(messageData);
        var msgId = message.msgId ? to4BytesBuffer(message.msgId) : "";
        var prefixBuffer = !!msgId ? Buffer.concat([to4BytesBuffer(length), msgId], 8) : to4BytesBuffer(length)
        var data = Buffer.concat([prefixBuffer, Buffer.from(messageData)]);
        console.log("Server is sending buffer: ", Buffer.from(data));
        return data;
    },

    _onClose: function() {
        this._closed = true;
    },
    _onConnect: function() {
        this._closed = false;
    },
    _onError: function() {
        this._closed = true;
    },
    isClosed: function() {
        return this._closed;
    }

};

var delegates = [
    'connect',
    'on',
    'end',
    'setTimeout',
    'setKeepAlive'
];
delegates.forEach(function(method) {
    JsonSocket.prototype[method] = function() {
        this._socket[method].apply(this._socket, arguments);
        return this
    }
});
