import { getClients } from "../state/clients";

type SendToClientProps = {
  agentId: number,
  data: any,
}

const sendToClient = (obj: SendToClientProps, cb?: (err?: Error) => void) => {
  const receiver = getClients().users.find(user => user.agentId === obj.agentId);
  receiver.socket.sendMessage(obj.data, cb);
}

const sendToClients = (data: any, cb?: (err?: Error) => void) => {
  getClients().users.map((user) => {
    if(user.isConnected) {
      user.socket.sendMessage(data, cb);
    }
  })
}

export { sendToClient, sendToClients }
